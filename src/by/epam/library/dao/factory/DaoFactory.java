package by.epam.library.dao.factory;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.OrderDao;
import by.epam.library.dao.StorageDao;
import by.epam.library.dao.UserDao;

/**
 * Created by Даша on 30.11.2015.
 */
public abstract class DaoFactory {
    public static final int MYSQL = 1;

    public abstract UserDao getUserDao();
    public abstract BookDao getBookDao();
    public abstract OrderDao getOrderDao();
    public abstract StorageDao getStorageDao();

    public static DaoFactory getDaoFactory(int factory) {
        switch (factory) {
            case MYSQL:
                return MySQLDaoFactory.getInstance();
            default:
                return null;
        }
    }
}
