package by.epam.library.dao.factory;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.OrderDao;
import by.epam.library.dao.StorageDao;
import by.epam.library.dao.UserDao;
import by.epam.library.dao.impl.MySQLBookDao;
import by.epam.library.dao.impl.MySQLOrderDao;
import by.epam.library.dao.impl.MySQLStorageDao;
import by.epam.library.dao.impl.MySQLUserDao;

/**
 * Created by ���� on 03.11.2015.
 */
public final class MySQLDaoFactory extends DaoFactory {
    private MySQLDaoFactory() {}

    private static class SingletonHelper {
        private static final MySQLDaoFactory instance = new MySQLDaoFactory();
    }

    public static MySQLDaoFactory getInstance() {
        return SingletonHelper.instance;
    }

    public UserDao getUserDao() {
        return MySQLUserDao.getInstance();
    }

    public BookDao getBookDao() {
        return MySQLBookDao.getInstance();
    }

    public OrderDao getOrderDao() {
        return MySQLOrderDao.getInstance();
    }

    public StorageDao getStorageDao() {
        return MySQLStorageDao.getInstance();
    }
}