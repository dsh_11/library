package by.epam.library.controller.command.librarian;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.User;
import by.epam.library.resource.Resource;
import by.epam.library.service.impl.librarian.ReturnBookServiceImpl;
import by.epam.library.service.librarian.ReturnBookService;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Даша on 16.11.2015.
 */
public class ReturnBookCommand implements Command {
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String FORWARD_PAGE_LOG_IN = "forward.page.log.in";
    private static final String FORWARD_PAGE_ORDERS_LIST = "forward.page.orders.list.issued";

    private static final String PARAM_USER = "user";
    private static final String PARAM_ORDER_ID = "orderId";
    private static final String PARAM_BOOK_ID = "bookId";
    private static final String ROLE = "librarian";

    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        User currentUser = (User)session.getAttribute(PARAM_USER);
        if (currentUser == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        if (!ROLE.equals(currentUser.getRole())) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        String parameterOrderId = request.getParameter(PARAM_ORDER_ID);
        boolean isNumber = Validator.validateNumber(parameterOrderId);
        if (!isNumber) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
        int orderId = Integer.valueOf(parameterOrderId);

        String parameterBookId = request.getParameter(PARAM_BOOK_ID);
        isNumber = Validator.validateNumber(parameterBookId);
        if (!isNumber) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
        int bookId = Integer.valueOf(parameterBookId);
        ReturnBookService service = ReturnBookServiceImpl.getInstance();
        service.returnBook(orderId, bookId);
        return Resource.getValue(FORWARD_PAGE_ORDERS_LIST);
    }
}
