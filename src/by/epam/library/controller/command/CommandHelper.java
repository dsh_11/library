package by.epam.library.controller.command;

import by.epam.library.controller.command.admin.*;
import by.epam.library.controller.command.authorization.LogInCommand;
import by.epam.library.controller.command.authorization.RegisterCommand;
import by.epam.library.controller.command.book.BookInfoCommand;
import by.epam.library.controller.command.book.CoversCommand;
import by.epam.library.controller.command.book.CatalogueCommand;
import by.epam.library.controller.command.librarian.ConfirmOrderCommand;
import by.epam.library.controller.command.librarian.IssueOrderCommand;
import by.epam.library.controller.command.librarian.OrdersListCommand;
import by.epam.library.controller.command.librarian.ReturnBookCommand;
import by.epam.library.controller.command.user.OrderBookCommand;
import by.epam.library.controller.command.user.SubmitOrderCommand;
import by.epam.library.controller.command.common.ChangeLangCommand;
import by.epam.library.controller.command.common.LogOutCommand;
import by.epam.library.controller.command.book.SearchCommand;
import by.epam.library.controller.command.user.CancelOrderCommand;
import by.epam.library.controller.command.user.OrderedBooksCommand;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ���� on 03.11.2015.
 */
public class CommandHelper {
    private Map<CommandName, Command> commands = new HashMap<>();

    public CommandHelper() {
        commands.put(CommandName.LOG_IN, new LogInCommand());
        commands.put(CommandName.REGISTER, new RegisterCommand());
        commands.put(CommandName.LOG_OUT, new LogOutCommand());
        commands.put(CommandName.CHANGE_LANG, new ChangeLangCommand());
        commands.put(CommandName.COVERS, new CoversCommand());
        commands.put(CommandName.USERS_LIST, new UsersListCommand());
        commands.put(CommandName.ADD_USER, new AddUserCommand());
        commands.put(CommandName.BOOK_INFO, new BookInfoCommand());
        commands.put(CommandName.ORDER_BOOK, new OrderBookCommand());
        commands.put(CommandName.SUBMIT_ORDER, new SubmitOrderCommand());
        commands.put(CommandName.ORDERED_BOOKS, new OrderedBooksCommand());
        commands.put(CommandName.ORDERS_LIST, new OrdersListCommand());
        commands.put(CommandName.CONFIRM_ORDER, new ConfirmOrderCommand());
        commands.put(CommandName.ISSUE_ORDER, new IssueOrderCommand());
        commands.put(CommandName.RETURN_BOOK, new ReturnBookCommand());
        commands.put(CommandName.CANCEL_ORDER, new CancelOrderCommand());
        commands.put(CommandName.SEARCH, new SearchCommand());
        commands.put(CommandName.CATALOGUE, new CatalogueCommand());
        commands.put(CommandName.ADD_BOOK, new AddBookCommand());
        commands.put(CommandName.EDIT_BOOK, new EditBookCommand());
        commands.put(CommandName.SUBMIT_EDIT_BOOK, new SubmitEditBookCommand());
        commands.put(CommandName.ADD_COPIES, new AddCopiesCommand());
        commands.put(CommandName.SUBMIT_ADD_COPIES, new SubmitAddCopiesCommand());
    }

    public Command getCommand(String commandName) {
        CommandName name = CommandName.valueOf(commandName.toUpperCase());
        return commands.get(name);
    }

    private enum CommandName {
        LOG_IN, REGISTER, LOG_OUT, CHANGE_LANG, COVERS, USERS_LIST,
        ADD_USER, BOOK_INFO, ORDER_BOOK, SUBMIT_ORDER, ORDERED_BOOKS,
        ORDERS_LIST, CONFIRM_ORDER, ISSUE_ORDER, RETURN_BOOK, CANCEL_ORDER,
        SEARCH, CATALOGUE, ADD_BOOK, EDIT_BOOK, SUBMIT_EDIT_BOOK, ADD_COPIES,
        SUBMIT_ADD_COPIES
    }
}
