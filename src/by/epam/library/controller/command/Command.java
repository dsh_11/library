package by.epam.library.controller.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ���� on 03.11.2015.
 */
public interface Command {
    String execute(HttpServletRequest request) throws CommandException;
}
