package by.epam.library.controller.command.common;

import by.epam.library.controller.command.Command;
import by.epam.library.resource.Resource;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ���� on 05.11.2015.
 */
public class LogOutCommand implements Command {
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String PARAM_LANG = "language";

    public String execute(HttpServletRequest request) {
        Object language = request.getSession(true).getAttribute(PARAM_LANG);
        request.getSession(true).invalidate();
        request.getSession(true).setAttribute(PARAM_LANG, language);
        return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
    }
}
