package by.epam.library.service.impl.librarian;

import by.epam.library.dao.OrderDao;
import by.epam.library.dao.StorageDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.service.ServiceException;
import by.epam.library.service.librarian.ReturnBookService;

/**
 * Created by Даша on 16.11.2015.
 */
public class ReturnBookServiceImpl implements ReturnBookService {
    private ReturnBookServiceImpl() {}

    private static class SingletonHelper {
        private static final ReturnBookServiceImpl instance = new ReturnBookServiceImpl();
    }

    public static ReturnBookServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public void returnBook(int orderId, int bookId) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        OrderDao orderDao = daoFactory.getOrderDao();
        StorageDao storageDao = daoFactory.getStorageDao();
        storageDao.unmarkBookAsOrdered(bookId);
        orderDao.removeOrder(orderId);
        orderDao.returnBookToStorage(bookId);
    }
}