package by.epam.library.service.impl.book;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.vo.BookVO;
import by.epam.library.service.ServiceException;
import by.epam.library.service.book.CatalogueService;

import java.util.List;

/**
 * Created by Даша on 21.11.2015.
 */
public class CatalogueServiceImpl implements CatalogueService {
    private static final int BOOKS_NUMBER_TO_SHOW = 6;

    private CatalogueServiceImpl() {}

    private static class SingletonHelper {
        private static final CatalogueServiceImpl instance = new CatalogueServiceImpl();
    }

    public static CatalogueServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public List<BookVO> getBooks(int page) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        BookDao bookDao = daoFactory.getBookDao();

        int offset = (page - 1) * BOOKS_NUMBER_TO_SHOW;
        return bookDao.getBooks(offset, BOOKS_NUMBER_TO_SHOW);
    }

    public int getPagesNumber() throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        BookDao bookDao = daoFactory.getBookDao();
        int booksNumber = bookDao.getBooksNumber();
        int pagesNumber = (int) Math.ceil((double) booksNumber / BOOKS_NUMBER_TO_SHOW);
        return pagesNumber;
    }
}
