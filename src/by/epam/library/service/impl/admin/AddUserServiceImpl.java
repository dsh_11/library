package by.epam.library.service.impl.admin;

import by.epam.library.dao.UserDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.User;
import by.epam.library.service.admin.AddUserService;
import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 12.11.2015.
 */
public class AddUserServiceImpl implements AddUserService {
    private AddUserServiceImpl() {}

    private static class SingletonHelper {
        private static final AddUserServiceImpl instance = new AddUserServiceImpl();
    }

    public static AddUserServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public boolean addUser(User user) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        UserDao userDao = daoFactory.getUserDao();

        String login = user.getLogin();
        Boolean loginExists = userDao.loginExists(login);
        if (!loginExists) {
            userDao.createUser(user);
            return true;
        } else {
            return false;
        }
    }
}