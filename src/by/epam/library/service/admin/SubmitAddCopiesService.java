package by.epam.library.service.admin;

import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 29.11.2015.
 */
public interface SubmitAddCopiesService {
    boolean addCopies(int bookId, int amount) throws ServiceException;
}
