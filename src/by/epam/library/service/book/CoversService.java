package by.epam.library.service.book;

import by.epam.library.service.ServiceException;

import java.util.Map;

/**
 * Created by Даша on 15.11.2015.
 */
public interface CoversService {
    Map<Integer, String> getBooksCovers() throws ServiceException;
}