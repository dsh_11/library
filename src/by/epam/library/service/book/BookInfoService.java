package by.epam.library.service.book;

import by.epam.library.domain.Book;
import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 15.11.2015.
 */
public interface BookInfoService {
    Book getBook(int id) throws ServiceException;
}
