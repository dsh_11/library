package by.epam.library.service.user;

import by.epam.library.domain.vo.UserOrderVO;
import by.epam.library.service.ServiceException;

import java.util.List;

/**
 * Created by Даша on 15.11.2015.
 */
public interface OrderedBooksService {
    List<UserOrderVO> getOrderedBooks(int userId, int page) throws ServiceException;
    int getPagesNumber(int userId) throws ServiceException;
}
