package by.epam.library.service.user;

import by.epam.library.domain.Order;
import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 15.11.2015.
 */
public interface SubmitOrderService {
    boolean submitOrder(Order order) throws ServiceException;
    boolean userHasOrder(Order order) throws ServiceException;
}
