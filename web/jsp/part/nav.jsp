<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 06.11.2015
  Time: 22:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="${pageContext.request.contextPath}/library?command=covers">Library</a>
    </div>

    <div class="collapse navbar-collapse">

      <ul class="nav navbar-nav">
        <li>
          <a href="${pageContext.request.contextPath}/library?command=covers"><fmt:message key="home.menu.home" /></a>
        </li>
        <li>
          <a href="${pageContext.request.contextPath}/library?command=catalogue"><fmt:message key="home.menu.catalogue" /></a>
        </li>
        <c:choose>
          <c:when test="${user.role == 'admin'}">
            <li>
              <a href="${pageContext.request.contextPath}/library?command=users_list"><fmt:message key="home.menu.users" /></a>
            </li>
          </c:when>
          <c:when test="${user.role == 'librarian'}">
            <li>
              <a href="${pageContext.request.contextPath}/library?command=orders_list&status=unconfirmed"><fmt:message key="home.menu.orders" /></a>
            </li>
          </c:when>
          <c:when test="${user.role == 'user'}">
            <li>
              <a href="${pageContext.request.contextPath}/library?command=ordered_books"><fmt:message key="home.menu.orders" /></a>
            </li>
          </c:when>
        </c:choose>
      </ul>

      <form class="navbar-form navbar-right" action="${pageContext.request.contextPath}/library" method="post">
        <input type="hidden" name="command" value="change_lang" />
        <c:if test="${language == 'ru'}">
          <input type="hidden" name="language" value="en" />
          <input class="btn btn-black" type="submit" value="en" />
        </c:if>
        <c:if test="${language == 'en'}">
          <input type="hidden" name="language" value="ru" />
          <input class="btn btn-black" type="submit" value="ru" />
        </c:if>
      </form>

      <ul class="nav navbar-nav navbar-right">
        <c:if test="${user == null}">
          <li><a href="${pageContext.request.contextPath}/login"><fmt:message key="authorization.log.in.title" /></a></li>
          <li><a href="${pageContext.request.contextPath}/register"><fmt:message key="authorization.sign.up.title" /></a></li>
        </c:if>
        <c:if test="${user != null}">
          <li style="float: left">
            <span><fmt:message key="greeting" />, ${user.login}</span>
          </li>
          <li style="float: right">
            <a href="${pageContext.request.contextPath}/library?command=log_out"><fmt:message key="authorization.log.out" /></a>
          </li>
        </c:if>
      </ul>
    </div>
  </div>
</nav>
