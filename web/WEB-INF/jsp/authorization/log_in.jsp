<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 05.11.2015
  Time: 14:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : 'en'}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="authorization.log.in.title" var="title"/>
<fmt:message key="authorization.login" var="login" />
<fmt:message key="authorization.password" var="password" />
<fmt:message key="authorization.log.in.message.login" var="loginMessage" />
<fmt:message key="authorization.log.in.message.password" var="passwordMessage" />
<fmt:message key="authorization.log.in.message.wrong.data" var="wrongDataMessage" />
<fmt:message key="button.cancel" var="cancelButton" />

<html>
  <head>
    <title>${title}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signin.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/validation.js"></script>
  </head>
  <body>
    <div class="container">
      <form class="form-signin" action="${pageContext.request.contextPath}/library" method="post" onsubmit="submitFormLogIn(this); return false;">
        <h2 class="form-signin-heading">${title}</h2>
        <div>
          <input class="form-control" type="text" name="login" id="loginInput" autocomplete="off" placeholder="${login}" />
        </div>
        <div>
          <input class="form-control" type="password" name="password" id="passwordInput" placeholder="${password}" />
        </div>
        <div class="message-box">
          <span class="message" id="loginMessage">${loginMessage}</span>
          <span class="message" id="passwordMessage">${passwordMessage}</span>
        </div>
        <c:if test="${wrongData}">
          <div>
              <span style="color: #a94442">${wrongDataMessage}</span>
          </div>
        </c:if>
        <input type="hidden" name="command" value="log_in" />
        <div>
          <input class="btn btn-lg btn-black btn-block" type="submit" value="${title}" />
          <a class="btn btn-lg btn-black btn-block" href="${pageContext.request.contextPath}/library">${cancelButton}</a>
        </div>
      </form>
    </div>
  </body>
</html>
