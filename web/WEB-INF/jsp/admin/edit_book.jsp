<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 27.11.2015
  Time: 22:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="add.book.name" var="name" />
<fmt:message key="add.book.author" var="author" />
<fmt:message key="add.book.pages.number" var="pagesNumber" />
<fmt:message key="add.book.description" var="description" />
<fmt:message key="add.book.wrong.data.message" var="wrongDataMessage" />
<fmt:message key="button.cancel" var="cancelButton" />
<fmt:message key="edit.book.title" var="title" />
<fmt:message key="edit.book.edit.button" var="editButton" />
<fmt:message key="add.book.name.message" var="nameMessage" />
<fmt:message key="add.book.author.message" var="authorMessage" />
<fmt:message key="add.book.pages.number.message" var="pagesNumberMessage" />

<html>
  <head>
    <title>${title} "${book.name}"</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signin.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/validation.js"></script>
  </head>
  <body>
    <div class="container">
      <form class="form-signin" action="${pageContext.request.contextPath}/library?command=submit_edit_book&bookId=${book.bookId}" method="post"
            onsubmit="submitEditBook(this); return false;" enctype="multipart/form-data">
        <h2 class="form-signin-heading" style="display: inline">${title}</h2>
        <img style="width: 30px; float: right" src="${book.cover}" />
        <div id="name">
          <input class="form-control" type="text" name="name" id="nameInput" placeholder="${name}" value="${book.name}" />
        </div>
        <div id="author">
          <input class="form-control" type="text" name="author" id="authorInput" placeholder="${author}" value="${book.author}" />
        </div>
        <div id="pagesNumber">
          <input class="form-control" type="text" name="pagesNumber" id="pagesNumberInput" placeholder="${pagesNumber}" value="${book.pagesNumber}" />
        </div>
        <div id="description">
          <textarea class="form-control" style="resize: none" rows="4" name="description" id="descriptionInput"
                    placeholder="${description}">${book.description}</textarea>
        </div>
        <div id="image">
          <input class="form-control" type="file" name="image" id="imageInput" />
        </div>
        <div class="message-box">
          <span class="message" id="nameMessage">${nameMessage}</span>
          <span class="message" id="authorMessage">${authorMessage}</span>
          <span class="message" id="pagesNumberMessage">${pagesNumberMessage}</span>
        </div>
        <c:if test="${wrongData}">
          <div>
            <span style="color: #a94442">${wrongDataMessage}</span>
          </div>
        </c:if>
        <div>
          <input class="btn btn-lg btn-black btn-block" type="submit" value="${editButton}" />
          <a class="btn btn-lg btn-black btn-block" href="${pageContext.request.contextPath}/library">${cancelButton}</a>
        </div>
        <input type="hidden" name="currentCover" value="${book.cover}" />
      </form>
    </div>
  </body>
</html>