package by.epam.library.dao.impl;

import by.epam.library.dao.UserDao;
import by.epam.library.dao.connectionpool.ConnectionPoolImpl;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.User;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Created by Даша on 24.11.2015.
 */
public class MySQLUserDaoTest {

    @BeforeClass
    public static void initialize() throws ConnectionPoolException {
        Locale.setDefault(Locale.ENGLISH);
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        connectionPool.initialize();
    }

    @Test
    public void testLoginExists() throws Exception {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        UserDao userDao = daoFactory.getUserDao();
        assertEquals(true, userDao.loginExists("dsh_11"));
    }

    @Test
    public void testGetUser() throws Exception {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        UserDao userDao = daoFactory.getUserDao();
        String login = "dsh_11";
        String password = "123456";
        User user = userDao.getUser(login, password);
        assertEquals("Daria", user.getFirstName());
        assertEquals("Evzhick", user.getLastName());
        assertEquals("dasha.evzhick@mail.ru", user.getEmail());
        assertEquals("admin", user.getRole());
        assertEquals(1, user.getUserId());
    }
}