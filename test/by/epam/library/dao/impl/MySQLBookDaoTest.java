package by.epam.library.dao.impl;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.connectionpool.ConnectionPoolImpl;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.Book;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Created by Даша on 24.11.2015.
 */
public class MySQLBookDaoTest {

    @BeforeClass
    public static void initialize() throws ConnectionPoolException {
        Locale.setDefault(Locale.ENGLISH);
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        connectionPool.initialize();
    }

    @Test
    public void testGetBook() throws Exception {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        BookDao bookDao = daoFactory.getBookDao();
        Book book = bookDao.getBook(1);
        assertEquals("The Lord of the Rings: The Fellowship of the Ring", book.getName());
        assertEquals("J.R.R. Tolkien", book.getAuthor());
        assertEquals(576, book.getPagesNumber());
        assertEquals("978-0007488308", book.getIsbn());
    }
}