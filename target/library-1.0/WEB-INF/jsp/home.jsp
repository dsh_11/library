<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 09.11.2015
  Time: 18:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="home.search.button" var="searchButton" />
<fmt:message key="home.search.input" var="searchInput" />
<fmt:message key="home.title" var="title" />

<html>
  <head>
<<<<<<< HEAD
    <title>${title}</title>  
    
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/stylesheet.css" />
    <script type="text/javascript" src="../js/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script type="text/javascript" src="../js/validation.js"></script>
=======
    <title>${title}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/validation.js"></script>
>>>>>>> library_temp
  </head>
  <body>
    <jsp:include page="../../jsp/part/nav.jsp" />
    <header class="header">
      <div class="container">
        <form class="form-inline form-search" action="${pageContext.request.contextPath}/library" method="post" onsubmit="submitSearchForm(this); return false;">
          <div class="form-group form-search-input">
            <input class="form-control" style="width: 100%" type="text" name="searchQuery" placeholder="${searchInput}" />
          </div>
          <div class="form-group form-search-button">
            <input type="hidden" name="command" value="search" />
            <input class="form-control btn btn-md btn-black btn-block" type="submit" value="${searchButton}" />
          </div>
        </form>
        <div class="books">
          <ul type="none" style="padding-left: 0">
            <c:forEach items="${covers}" var="cover">
              <li class="book">
                <a href="${pageContext.request.contextPath}/library?command=book_info&bookId=${cover.key}">
                  <img width="120px" src="${cover.value}" />
                </a>
              </li>
            </c:forEach>
          </ul>
        </div>
      </div>
    </header>
  </body>
</html>