<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 29.11.2015
  Time: 17:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="add.book.button.add" var="addButton" />
<fmt:message key="button.cancel" var="cancelButton" />
<fmt:message key="add.copies.amount.placeholder" var="amountPlaceholder" />
<fmt:message key="add.copies.message.book" var="messageBook" />
<fmt:message key="add.copies.title" var="title" />
<fmt:message key="add.book.amount.message" var="amountMessage" />
<fmt:message key="add.book.wrong.data.message" var="wrongDataMessage" />

<html>
  <head>
      <title>${title}</title>
      <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
      <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
      <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signin.css" />
      <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/js/validation.js"></script>
  </head>
  <body>
    <div class="container">
      <form class="form-signin" action="${pageContext.request.contextPath}/library" method="post">
        <div class="order-header">
          <span><b>${messageBook}:<br>${bookName}</b></span>
        </div>
        <div>
          <input class="form-control" type="text" name="amount" placeholder="${amountPlaceholder}" />
        </div>
        <c:if test="${wrongData}">
          <div>
            <span style="color: #a94442">${wrongDataMessage}</span>
          </div>
        </c:if>
        <div>
          <input type="hidden" name="bookId" value="${bookId}" />
          <input type="hidden" name="bookName" value="${bookName}" />
          <input type="hidden" name="command" value="submit_add_copies" />
          <input class="btn btn-lg btn-black btn-block" type="submit" value="${addButton}" />
          <a class="btn btn-lg btn-black btn-block" href="${pageContext.request.contextPath}/library">${cancelButton}</a>
        </div>
      </form>
    </div>
  </body>
</html>
