<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 12.11.2015
  Time: 10:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="users.title" var="title" />
<fmt:message key="users.login" var="login" />
<fmt:message key="users.first.name" var="firstName" />
<fmt:message key="users.last.name" var="lastName" />
<fmt:message key="users.email" var="email" />
<fmt:message key="users.role" var="role" />
<fmt:message key="users.button.add.user" var="addUserButton" />

<html>
  <head>
    <title>${title}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
    <script src="${pageContext.request.contextPath}/js/jquery-2.1.4.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/query.js"></script>
  </head>
  <body>
    <jsp:include page="/jsp/part/nav.jsp" />
    <header class="header">
      <div class="container">
        <div style="margin-top: 40px; margin-bottom: 20px; float: right;">
          <a class="btn btn-md btn-black" href="${pageContext.request.contextPath}/add-user">${addUserButton}</a>
        </div>
        <div class="list">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>${login}</th>
                <th>${firstName}</th>
                <th>${lastName}</th>
                <th>${email}</th>
                <th>${role}</th>
              </tr>
            </thead>
            <tbody>
              <c:forEach items="${users}" var="user">
                <tr>
                  <td>${user.login}</td>
                  <td>${user.firstName}</td>
                  <td>${user.lastName}</td>
                  <td>${user.email}</td>
                  <td>${user.role}</td>
                </tr>
              </c:forEach>
            </tbody>
          </table>
        </div>

        <div class="pages">
          <ul class="pagination">
            <c:forEach begin="1" end="${pagesNumber}" var="i">
              <c:choose>
                <c:when test="${currentPage == i}">
                  <li class="active">
                    <span id="page-active">${i}</span>
                  </li>
                </c:when>
                <c:otherwise>
                  <li>
                    <a id="page-inactive" href="${pageContext.request.contextPath}/library?command=users_list&page=${i}">${i}</a>
                  </li>
                </c:otherwise>
              </c:choose>
            </c:forEach>
          </ul>
        </div>
      </div>
    </header>
  </body>
</html>
