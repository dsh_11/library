<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 13.11.2015
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="order.book.title" var="title" />
<fmt:message key="order.book.message.order" var="messageOrder" />
<fmt:message key="order.book.message.place" var="messagePlace" />
<fmt:message key="order.book.place.home" var="placeHome" />
<fmt:message key="order.book.place.reading.room" var="placeReadingRoom" />
<fmt:message key="order.book.button.order" var="orderButton" />
<fmt:message key="order.book.message.one.copy" var="oneCopyMessage" />
<fmt:message key="order.book.message.no.copies" var="noCopiesMessage" />
<fmt:message key="button.cancel" var="cancelButton" />

<html>
<head>
  <title>${title}</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signin.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
  <script src="${pageContext.request.contextPath}/js/query.js"></script>
</head>
<body>
  <header class="header">
    <div class="container">
      <form class="form-signin" action="${pageContext.request.contextPath}/library" method="post" onsubmit="submitOrder(this); return false;">
        <div class="order-header">
          <span><b>${messageOrder}:<br>${bookName}</b></span>
        </div>
        <div>
          ${messagePlace}:
          <label class="radio-inline">
            <input type="radio" name="optradio" value="home" checked="checked">${placeHome}
          </label>
          <label class="radio-inline">
            <input type="radio" name="optradio" value="reading room">${placeReadingRoom}
          </label>
        </div>
        <div class="message-box">
          <c:if test="${userHasOrder}">
            <span class="message" style="display: block">${oneCopyMessage}</span>
          </c:if>
          <c:if test="${noCopies}">
            <span class="message" style="display: block">${noCopiesMessage}</span>
          </c:if>
        </div>
        <input type="hidden" name="bookId" value="${bookId}" />
        <input type="hidden" name="bookName" value="${bookName}" />
        <input type="hidden" name="orderType" />
        <input type="hidden" name="command" value="submit_order" />
        <div>
          <input class="btn btn-lg btn-black btn-block" type="submit" value="${orderButton}" />
          <a class="btn btn-lg btn-black btn-block" href="${pageContext.request.contextPath}/library">${cancelButton}</a>
        </div>
      </form>
    </div>
  </header>
</body>
</html>
