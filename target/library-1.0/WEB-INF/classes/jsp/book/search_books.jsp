<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 21.11.2015
  Time: 15:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="home.search.button" var="searchButton" />
<fmt:message key="home.search.input" var="searchInput" />

<html>
  <head>
    <title>Search results for ${searchQuery}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/validation.js"></script>
  </head>
  <body>
    <jsp:include page="../part/nav.jsp" />
    <header class="header">
      <div class="container">
        <jsp:include page="../part/books.jsp" />
        <div class="pages">
          <ul class="pagination">
            <c:forEach begin="1" end="${pagesNumber}" var="i">
              <c:choose>
                <c:when test="${currentPage == i}">
                  <li class="active">
                    <span id="page-active">${i}</span>
                  </li>
                </c:when>
                <c:otherwise>
                  <li>
                    <a id="page-inactive" href="${pageContext.request.contextPath}/library?command=search&searchQuery=${searchQuery}&page=${i}">${i}</a>
                  </li>
                </c:otherwise>
              </c:choose>
            </c:forEach>
          </ul>
        </div>
      </div>
    </header>
  </body>
</html>
