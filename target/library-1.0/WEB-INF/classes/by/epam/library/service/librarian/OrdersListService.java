package by.epam.library.service.librarian;

import by.epam.library.domain.vo.LibrarianOrderVO;
import by.epam.library.service.ServiceException;

import java.util.List;

/**
 * Created by Даша on 15.11.2015.
 */
public interface OrdersListService {
    List<LibrarianOrderVO> getOrders(int page, String status) throws ServiceException;
    int getPagesNumber(String status) throws ServiceException;
}
