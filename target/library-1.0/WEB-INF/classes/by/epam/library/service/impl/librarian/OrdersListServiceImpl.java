package by.epam.library.service.impl.librarian;

import by.epam.library.dao.OrderDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.vo.LibrarianOrderVO;
import by.epam.library.service.ServiceException;
import by.epam.library.service.librarian.OrdersListService;

import java.util.List;

/**
 * Created by Даша on 15.11.2015.
 */
public class OrdersListServiceImpl implements OrdersListService {
    private static final int ORDERS_NUMBER_TO_SHOW = 6;

    private OrdersListServiceImpl() {}

    private static class SingletonHelper {
        private static final OrdersListServiceImpl instance = new OrdersListServiceImpl();
    }

    public static OrdersListServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public List<LibrarianOrderVO> getOrders(int page, String status) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        OrderDao orderDao = daoFactory.getOrderDao();
        int offset = (page - 1) * ORDERS_NUMBER_TO_SHOW;
        return orderDao.getOrders(offset, ORDERS_NUMBER_TO_SHOW, status);
    }

    public int getPagesNumber(String status) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        OrderDao orderDao = daoFactory.getOrderDao();
        int ordersNumber = orderDao.getOrdersNumber(status);
        int pagesNumber = (int) Math.ceil((double) ordersNumber / ORDERS_NUMBER_TO_SHOW);
        return pagesNumber;
    }
}
