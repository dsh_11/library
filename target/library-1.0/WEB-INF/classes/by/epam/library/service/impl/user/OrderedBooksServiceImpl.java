package by.epam.library.service.impl.user;

import by.epam.library.dao.OrderDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.vo.UserOrderVO;
import by.epam.library.service.ServiceException;
import by.epam.library.service.user.OrderedBooksService;

import java.util.List;

/**
 * Created by Даша on 15.11.2015.
 */
public class OrderedBooksServiceImpl implements OrderedBooksService {
    private static final int ORDERS_NUMBER_TO_SHOW = 6;

    private OrderedBooksServiceImpl() {}

    private static final class SingletonHelper {
        private static final OrderedBooksServiceImpl instance = new OrderedBooksServiceImpl();
    }

    public static OrderedBooksServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public List<UserOrderVO> getOrderedBooks(int userId, int page) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        OrderDao orderDao = daoFactory.getOrderDao();
        int offset = (page - 1) * ORDERS_NUMBER_TO_SHOW;
        return orderDao.getUserOrders(offset, ORDERS_NUMBER_TO_SHOW, userId);
    }

    public int getPagesNumber(int userId) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        OrderDao orderDao = daoFactory.getOrderDao();
        int ordersNumber = orderDao.getOrdersNumber(userId);
        int pagesNumber = (int) Math.ceil((double) ordersNumber / ORDERS_NUMBER_TO_SHOW);
        return pagesNumber;
    }
}