package by.epam.library.service.impl.admin;

import by.epam.library.dao.StorageDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.service.ServiceException;
import by.epam.library.service.admin.SubmitAddCopiesService;

/**
 * Created by Даша on 29.11.2015.
 */
public class SubmitAddCopiesServiceImpl implements SubmitAddCopiesService {
    private SubmitAddCopiesServiceImpl() {}

    private static class SingletonHelper {
        private static final SubmitAddCopiesServiceImpl instance = new SubmitAddCopiesServiceImpl();
    }

    public static SubmitAddCopiesServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public boolean addCopies(int bookId, int amount) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        StorageDao storageDao = daoFactory.getStorageDao();
        return storageDao.addCopies(bookId, amount);
    }
}
