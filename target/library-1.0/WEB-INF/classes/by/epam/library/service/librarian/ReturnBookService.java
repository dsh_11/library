package by.epam.library.service.librarian;

import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 16.11.2015.
 */
public interface ReturnBookService {
    void returnBook(int orderId, int bookId) throws ServiceException;
}
