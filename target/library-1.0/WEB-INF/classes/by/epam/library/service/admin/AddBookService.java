package by.epam.library.service.admin;

import by.epam.library.domain.Book;
import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 22.11.2015.
 */
public interface AddBookService {
    void addBook(Book book) throws ServiceException;
}
