package by.epam.library.service.admin;

import by.epam.library.domain.User;
import by.epam.library.service.ServiceException;

import java.util.List;

/**
 * Created by Даша on 15.11.2015.
 */
public interface UsersListService {
    List<User> getUsers(int page) throws ServiceException;
    int getPagesNumber() throws ServiceException;
}
