package by.epam.library.service.impl.admin;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.StorageDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.admin.AddBookService;

/**
 * Created by Даша on 22.11.2015.
 */
public class AddBookServiceImpl implements AddBookService {
    private AddBookServiceImpl() {}

    private static class SingletonHelper {
        private static final AddBookServiceImpl instance = new AddBookServiceImpl();
    }

    public static AddBookServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public void addBook(Book book) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        BookDao bookDao = daoFactory.getBookDao();
        StorageDao storageDao = daoFactory.getStorageDao();

        boolean added = storageDao.addItem(book.getIsbn(), book.getAmount());
        if (added) {
            bookDao.createBook(book);
        }
    }
}
