package by.epam.library.service.user;

import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 19.11.2015.
 */
public interface CancelOrderService {
    void cancelOrder(int orderId, int bookId) throws ServiceException;
}
