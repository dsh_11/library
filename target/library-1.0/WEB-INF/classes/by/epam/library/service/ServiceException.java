package by.epam.library.service;

import by.epam.library.controller.command.CommandException;

/**
 * Created by ���� on 03.11.2015.
 */
public class ServiceException extends CommandException {
    private static final long serialVersionUID = 1L;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Exception e) {
        super(message, e);
    }
}
