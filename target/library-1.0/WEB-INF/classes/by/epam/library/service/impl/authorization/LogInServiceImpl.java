package by.epam.library.service.impl.authorization;

import by.epam.library.dao.UserDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.User;
import by.epam.library.service.authorization.LogInService;
import by.epam.library.service.ServiceException;

/**
 * Created by ���� on 03.11.2015.
 */
public class LogInServiceImpl implements LogInService {
    private LogInServiceImpl() {}

    private static class SingletonHelper {
        private static final LogInServiceImpl instance = new LogInServiceImpl();
    }

    public static LogInServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public User logIn(User user) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        UserDao userDao = daoFactory.getUserDao();
        String login = user.getLogin();
        String password = user.getPassword();
        return userDao.getUser(login, password);
    }
}
