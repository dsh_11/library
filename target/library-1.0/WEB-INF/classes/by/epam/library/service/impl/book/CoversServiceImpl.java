package by.epam.library.service.impl.book;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.service.ServiceException;
import by.epam.library.service.book.CoversService;

import java.util.Map;

/**
 * Created by Даша on 08.11.2015.
 */
public class CoversServiceImpl implements CoversService {
    private static final int BOOKS_NUMBER_TO_SHOW = 8;

    private CoversServiceImpl() {}

    private static class SingletonHelper {
        private static final CoversServiceImpl instance = new CoversServiceImpl();
    }

    public static CoversServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public Map<Integer, String> getBooksCovers() throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        BookDao bookDao = daoFactory.getBookDao();
        return bookDao.getBooksCovers(BOOKS_NUMBER_TO_SHOW);
    }
}
