package by.epam.library.service.impl.authorization;

import by.epam.library.dao.UserDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.authorization.RegisterService;

/**
 * Created by ���� on 03.11.2015.
 */
public class RegisterServiceImpl implements RegisterService {
    private static final String ROLE = "user";

    private RegisterServiceImpl() {}

    private static class SingletonHelper {
        private static final RegisterServiceImpl instance = new RegisterServiceImpl();
    }

    public static RegisterServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public boolean register(User user) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        UserDao userDao = daoFactory.getUserDao();
        String login = user.getLogin();
        Boolean loginExists = userDao.loginExists(login);
        if (!loginExists) {
            if (user.getRole() == null) {
                user.setRole(ROLE);
            }
            userDao.createUser(user);
            return false;
        }
        return true;
    }
}
