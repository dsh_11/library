package by.epam.library.service.book;

import by.epam.library.domain.vo.BookVO;
import by.epam.library.service.ServiceException;

import java.util.List;

/**
 * Created by Даша on 21.11.2015.
 */
public interface SearchService {
    List<BookVO> search(String query, int page) throws ServiceException;
    int getPagesNumber(String query) throws ServiceException;
}
