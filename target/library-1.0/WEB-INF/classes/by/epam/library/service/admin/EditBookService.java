package by.epam.library.service.admin;

import by.epam.library.domain.Book;
import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 27.11.2015.
 */
public interface EditBookService {
    Book getBook(int bookId) throws ServiceException;
    boolean editBook(Book book) throws ServiceException;
}
