package by.epam.library.dao;

import by.epam.library.domain.Order;
import by.epam.library.domain.vo.LibrarianOrderVO;
import by.epam.library.domain.vo.UserOrderVO;

import java.util.Date;
import java.util.List;

/**
 * Created by Даша on 14.11.2015.
 */
public interface OrderDao {
    boolean createOrder(Order order) throws DaoException;
    List<UserOrderVO> getUserOrders(int offset, int number, int userId) throws DaoException;
    List<LibrarianOrderVO> getOrders(int offset, int number, String status) throws DaoException;
    int getOrdersNumber(int userId) throws DaoException;
    int getOrdersNumber(String status) throws DaoException;
    void confirmOrder(int orderId) throws DaoException;
    void issueOrder(int orderId, Date issueDate, Date returnDate) throws DaoException;
    String getOrderType(int orderId) throws DaoException;
    boolean returnBookToStorage(int bookId) throws DaoException;
    void removeOrder(int orderId) throws DaoException;
    boolean userHasOrder(Order order) throws DaoException;
}