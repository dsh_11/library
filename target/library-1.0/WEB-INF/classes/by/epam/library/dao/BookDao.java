package by.epam.library.dao;

import by.epam.library.domain.Book;
import by.epam.library.domain.vo.BookVO;

import java.util.List;
import java.util.Map;

/**
 * Created by Даша on 14.11.2015.
 */
public interface BookDao {
    Map<Integer, String> getBooksCovers(int number) throws DaoException;
    int getBooksNumber() throws DaoException;
    Book getBook(int id) throws DaoException;
    List<BookVO> search(String query, int offset, int number) throws DaoException;
    int getSearchedBooksNumber(String query) throws DaoException;
    List<BookVO> getBooks(int offset, int number) throws DaoException;
    boolean createBook(Book book) throws DaoException;
    boolean editBook(Book book) throws DaoException;
}
