package by.epam.library.dao.impl;

import by.epam.library.dao.DaoException;
import by.epam.library.dao.OrderDao;
import by.epam.library.dao.connectionpool.ConnectionPoolImpl;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.domain.Order;
import by.epam.library.domain.vo.LibrarianOrderVO;
import by.epam.library.domain.vo.UserOrderVO;
import by.epam.library.resource.Resource;

import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by Даша on 12.11.2015.
 */
public class MySQLOrderDao implements OrderDao {
    private static final String SQL_CREATE_ORDER = "sql.create.order";
    private static final String SQL_GET_USER_ORDERS = "sql.get.user.orders";
    private static final String SQL_GET_USER_ORDERS_NUMBER = "sql.get.user.orders.number";
    private static final String SQL_GET_ORDERS = "sql.get.orders";
    private static final String SQL_GET_ORDERS_NUMBER = "sql.get.orders.number";
    private static final String SQL_CONFIRM_ORDER = "sql.confirm.order";
    private static final String SQL_ISSUE_ORDER = "sql.issue.order";
    private static final String SQL_GET_ORDER_TYPE = "sql.get.order.type";
    private static final String SQL_REMOVE_ORDER = "sql.remove.order";
    private static final String SQL_RETURN_BOOK_TO_STORAGE = "sql.return.book.to.storage";
    private static final String SQL_USER_HAS_ORDER = "sql.user.has.order";

    private MySQLOrderDao() {}

    private static class SingletonHelper {
        private static final MySQLOrderDao instance = new MySQLOrderDao();
    }

    public static MySQLOrderDao getInstance() {
        return SingletonHelper.instance;
    }

    /**
     * Creates order and returns <code>true</code>
     * if it was created.
     *
     * @param order order to be created
     * @return <code>true</code> if order was created
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public boolean createOrder(Order order) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_CREATE_ORDER))) {
            statement.setInt(1, order.getBookId());
            statement.setInt(2, order.getUserId());
            if (order.getIssueDate() != null) {
                statement.setDate(3, new java.sql.Date(order.getIssueDate().getTime()));
            } else {
                statement.setDate(3, null);
            }
            if (order.getReturnDate() != null) {
                statement.setDate(4, new java.sql.Date(order.getReturnDate().getTime()));
            } else {
                statement.setDate(4, null);
            }
            statement.setString(5, order.getStatus());
            statement.setString(6, order.getType());
            int result = statement.executeUpdate();
            return (result > 0);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns a list of orders of user with id <code>userId</code>.
     *
     * @param offset offset in the database from which orders should be returned
     * @param number number of orders to be returned
     * @param userId id of user which orders should be returned
     * @return a list of orders by user id
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public List<UserOrderVO> getUserOrders(int offset, int number, int userId) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        List<UserOrderVO> orders = new ArrayList<>();
        UserOrderVO order;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_USER_ORDERS))) {
            statement.setInt(1, userId);
            statement.setInt(2, offset);
            statement.setInt(3, number);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String bookName = resultSet.getString(1);
                String status = resultSet.getString(2);
                String type = resultSet.getString(3);
                Date issueDate = resultSet.getDate(4);
                Date returnDate = resultSet.getDate(5);
                int orderId = resultSet.getInt(6);
                int bookId = resultSet.getInt(7);
                order = new UserOrderVO();
                order.setBookName(bookName);
                order.setStatus(status);
                order.setType(type);
                order.setIssueDate(issueDate);
                order.setReturnDate(returnDate);
                order.setOrderId(orderId);
                order.setBookId(bookId);
                orders.add(order);
            }
            return orders;
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns a list of orders with status <code>status</code>.
     *
     * @param offset offset in the database from which orders should be returned
     * @param number number of orders to be returned
     * @param status status of orders to be returned
     * @return a list of orders by status
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public List<LibrarianOrderVO> getOrders(int offset, int number, String status) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        List<LibrarianOrderVO> orders = new ArrayList<>();
        LibrarianOrderVO order;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_ORDERS))) {
            statement.setString(1, status);
            statement.setInt(2, offset);
            statement.setInt(3, number);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String login = resultSet.getString(1);
                String bookName = resultSet.getString(2);
                String type = resultSet.getString(3);
                Date issueDate = resultSet.getDate(4);
                Date returnDate = resultSet.getDate(5);
                int bookId = resultSet.getInt(6);
                int orderId = resultSet.getInt(7);
                order = new LibrarianOrderVO();
                order.setUserLogin(login);
                order.setBookName(bookName);
                order.setStatus(status);
                order.setType(type);
                order.setIssueDate(issueDate);
                order.setReturnDate(returnDate);
                order.setBookId(bookId);
                order.setOrderId(orderId);
                orders.add(order);
            }
            return orders;
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns number of orders of user with id <code>userId</code>.
     *
     * @param userId id of user which orders should be returned
     * @return number of orders by user id
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public int getOrdersNumber(int userId) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_USER_ORDERS_NUMBER))) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns number of orders with status <code>status</code>.
     *
     * @param status status of orders to be returned
     * @return number of orders by status
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public int getOrdersNumber(String status) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_ORDERS_NUMBER))) {
            statement.setString(1, status);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Confirms order with id <code>orderId</code>.
     *
     * @param orderId id of order to be confirmed
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public void confirmOrder(int orderId) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_CONFIRM_ORDER))) {
            statement.setInt(1, orderId);
            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Issues order with id <code>orderId</code>
     * setting issued date to <code>issueDate</code>
     * and return date to <code>returnDate</code>.
     *
     * @param orderId id of order to be issued
     * @param issueDate issued date of order
     * @param returnDate return date of order
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public void issueOrder(int orderId, Date issueDate, Date returnDate) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_ISSUE_ORDER))) {
            statement.setDate(1, new java.sql.Date(issueDate.getTime()));
            statement.setDate(2, new java.sql.Date(returnDate.getTime()));
            statement.setInt(3, orderId);
            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns type of order with id <code>orderId</code>.
     *
     * @param orderId id of order which type should be returned
     * @return type of order with id <code>orderId</code>
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public String getOrderType(int orderId) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_ORDER_TYPE))) {
            statement.setInt(1, orderId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString(1);
            }
            return null;
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Increments number of books in storage with book id <code>bookId</code>.
     *
     * @param bookId id of book
     * @return <code>true</code> if book was returned
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public boolean returnBookToStorage(int bookId) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_RETURN_BOOK_TO_STORAGE))) {
            statement.setInt(1, bookId);
            int result = statement.executeUpdate();
            return (result > 0);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Removes the order with id <code>orderId</code>
     *
     * @param orderId id of order that should be removed
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public void removeOrder(int orderId) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_REMOVE_ORDER))) {
            statement.setInt(1, orderId);
            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns <code>true</code> if user has order <code>order</code>.
     *
     * @param order order that should be removed
     * @return <code>true</code> if user has order <code>order</code>
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public boolean userHasOrder(Order order) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_USER_HAS_ORDER))) {
            statement.setInt(1, order.getUserId());
            statement.setInt(2, order.getBookId());
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }
}
