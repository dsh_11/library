package by.epam.library.dao.impl;

import by.epam.library.dao.DaoException;
import by.epam.library.dao.StorageDao;
import by.epam.library.dao.connectionpool.ConnectionPool;
import by.epam.library.dao.connectionpool.ConnectionPoolImpl;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.resource.Resource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Даша on 13.11.2015.
 */
public class MySQLStorageDao implements StorageDao {
    private static final String SQL_TAKE_BOOK_FROM_STORAGE = "sql.take.book.from.storage";
    private static final String SQL_MARK_BOOK_AS_ORDERED = "sql.mark.book.as.ordered";
    private static final String SQL_UNMARK_BOOK_AS_ORDERED = "sql.unmark.book.as.ordered";
    private static final String SQL_GET_FREE_BOOKS_NUMBER = "sql.get.free.books.number";
    private static final String SQL_ADD_ITEM = "sql.add.item";
    private static final String SQL_ADD_COPIES = "sql.add.copies";

    private MySQLStorageDao() {}

    private static class SingletonHelper {
        private static final MySQLStorageDao instance = new MySQLStorageDao();
    }

    public static MySQLStorageDao getInstance() {
        return SingletonHelper.instance;
    }

    /**
     * Decrements number of books in storage where book is in order with <code>orderId</code>.
     *
     * @param orderId id of order that should be taken from storage
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public void takeBookFromStorage(int orderId) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_TAKE_BOOK_FROM_STORAGE))) {
            statement.setInt(1, orderId);
            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Decrements number of free books with book id <code>bookId</code>.
     *
     * @param bookId id of book that should be marked as ordered
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public void markBookAsOrdered(int bookId) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_MARK_BOOK_AS_ORDERED))) {
            statement.setInt(1, bookId);
            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Increments number of free books with book id <code>bookId</code>.
     *
     * @param bookId id of book that should be unmarked as ordered
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public void unmarkBookAsOrdered(int bookId) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_UNMARK_BOOK_AS_ORDERED))) {
            statement.setInt(1, bookId);
            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns number of books available for ordering with book id <code>bookId</code>.
     *
     * @param bookId id of book which number should be returned
     * @return number of books available for ordering
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public int getFreeBooksNumber(int bookId) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_FREE_BOOKS_NUMBER))) {
            statement.setInt(1, bookId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            return -1;
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Adds item with isbn <code>isbn</code> and amount <code>amount</code> to storage.
     *
     * @param isbn isbn of item to be added
     * @param amount amount of items with isbn <code>isbn</code>
     * @return <code>true</code> if item was added
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public boolean addItem(String isbn, int amount) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_ADD_ITEM))) {
            statement.setString(1, isbn);
            statement.setInt(2, amount);
            statement.setInt(3, amount);
            statement.setInt(4, amount);
            int result = statement.executeUpdate();
            return (result > 0);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    public boolean addCopies(int bookId, int amount) throws DaoException {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_ADD_COPIES))) {
            statement.setInt(1, amount);
            statement.setInt(2, amount);
            statement.setInt(3, amount);
            statement.setInt(4, bookId);
            int result = statement.executeUpdate();
            return (result > 0);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }
}
