package by.epam.library.dao.connectionpool;

import java.sql.Connection;

/**
 * Created by Даша on 26.11.2015.
 */
public interface ConnectionPool {
    void initialize() throws ConnectionPoolException;
    Connection takeConnection() throws ConnectionPoolException;
    void dispose() throws ConnectionPoolException;
}