package by.epam.library.dao.impl;

import by.epam.library.dao.DaoException;
import by.epam.library.dao.UserDao;
import by.epam.library.dao.connectionpool.ConnectionPoolImpl;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.domain.User;
import by.epam.library.resource.Resource;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ���� on 03.11.2015.
 */
public class MySQLUserDao implements UserDao {
    private static final String SQL_CREATE_USER = "sql.create.user";
    private static final String SQL_LOGIN_EXISTS = "sql.login.exists";
    private static final String SQL_GET_USER = "sql.get.user";
    private static final String SQL_GET_USERS_LIST = "sql.get.users.list";
    private static final String SQL_GET_USERS_NUMBER = "sql.get.users.number";

    private MySQLUserDao() {}

    private static class SingletonHelper {
        private static final MySQLUserDao instance = new MySQLUserDao();
    }

    public static MySQLUserDao getInstance() {
        return SingletonHelper.instance;
    }

    /**
     * Creates user <code>user</code>.
     *
     * @param user user to be created
     * @return <code>true</code> if user was created
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public boolean createUser(User user) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_CREATE_USER))) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getFirstName());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getRole());
            int result = statement.executeUpdate();
            return (result > 0);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns <code>true</code> if user with login <code>login</code> exists.
     *
     * @param login login to be checked
     * @return <code>true</code> if user with login <code>login</code> exists
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public boolean loginExists(String login) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_LOGIN_EXISTS))) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns user with login <code>login</code> and
     *         password <code>password</code> or <code>null</code>
     *         if such user doesn't exist.
     *
     * @param login user login
     * @param password user password
     * @return user with login <code>login</code> and
     *         password <code>password</code> or <code>null</code>
     *         if such user doesn't exist
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public User getUser(String login, String password) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        User user = null;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_USER))) {
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int id = resultSet.getInt(1);
                String firstName = resultSet.getString(2);
                String lastName = resultSet.getString(3);
                String email = resultSet.getString(4);
                String role = resultSet.getString(5);
                user = new User();
                user.setUserId(id);
                user.setLogin(login);
                user.setPassword(password);
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setRole(role);
            }
            return user;
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns a list of users in number of <code>number</code>.
     *
     * @param offset offset in the database from which users should be returned
     * @param number number of users to be returned
     * @return a list of users
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public List<User> getUsersList(int offset, int number) throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        List<User> users = new ArrayList<>();
        User user;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_USERS_LIST))) {
            statement.setInt(1, offset);
            statement.setInt(2, number);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String login = resultSet.getString(2);
                String firstName = resultSet.getString(3);
                String lastName = resultSet.getString(4);
                String email = resultSet.getString(5);
                String role = resultSet.getString(6);
                user = new User();
                user.setUserId(id);
                user.setLogin(login);
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setRole(role);
                users.add(user);
            }
            return users;
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns total number of users in the database.
     *
     * @return total number of users in the database
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public int getUsersNumber() throws DaoException {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(Resource.getValue(SQL_GET_USERS_NUMBER));
            resultSet.next();
            return resultSet.getInt(1);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }
}