package by.epam.library.dao;

/**
 * Created by Даша on 14.11.2015.
 */
public interface StorageDao {
    void takeBookFromStorage(int orderId) throws DaoException;
    void markBookAsOrdered(int bookId) throws DaoException;
    void unmarkBookAsOrdered(int bookId) throws DaoException;
    int getFreeBooksNumber(int bookId) throws DaoException;
    boolean addItem(String isbn, int amount) throws DaoException;
    boolean addCopies(int bookId, int amount) throws DaoException;
   // boolean editStorage(String isbn, int amount) throws DaoException;
}