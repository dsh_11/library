package by.epam.library.dao;

import by.epam.library.domain.User;

import java.util.List;

/**
 * Created by Даша on 14.11.2015.
 */
public interface UserDao {
    boolean createUser(User user) throws DaoException;
    boolean loginExists(String login) throws DaoException;
    User getUser(String login, String password) throws DaoException;
    List<User> getUsersList(int offset, int number) throws DaoException;
    int getUsersNumber() throws DaoException;
}
