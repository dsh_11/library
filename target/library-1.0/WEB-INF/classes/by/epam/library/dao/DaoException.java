package by.epam.library.dao;

import by.epam.library.service.ServiceException;

/**
 * Created by ���� on 03.11.2015.
 */
public class DaoException extends ServiceException {
    private static final long serialVersionUID = 1L;

    public DaoException(String message) {
        super(message);
    }

    public DaoException(String message, Exception e) {
        super(message, e);
    }
}
