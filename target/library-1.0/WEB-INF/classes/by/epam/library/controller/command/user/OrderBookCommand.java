package by.epam.library.controller.command.user;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.User;
import by.epam.library.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Даша on 12.11.2015.
 */
public class OrderBookCommand implements Command {
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String FORWARD_PAGE_LOG_IN = "forward.page.log.in";
    private static final String FORWARD_PAGE_ORDER_BOOK = "forward.page.order.book";

    private static final String PARAM_USER = "user";
    private static final String PARAM_BOOK_NAME = "bookName";
    private static final String PARAM_BOOK_ID = "bookId";
    private static final String ROLE = "user";

    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        User currentUser = (User) session.getAttribute(PARAM_USER);
        if (currentUser == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        if (!ROLE.equals(currentUser.getRole())) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        String bookName = request.getParameter(PARAM_BOOK_NAME);
        String bookId = request.getParameter(PARAM_BOOK_ID);
        request.setAttribute(PARAM_BOOK_NAME, bookName);
        request.setAttribute(PARAM_BOOK_ID, bookId);
        return Resource.getValue(FORWARD_PAGE_ORDER_BOOK);
    }
}
