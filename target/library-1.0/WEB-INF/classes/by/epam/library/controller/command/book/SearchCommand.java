package by.epam.library.controller.command.book;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.vo.BookVO;
import by.epam.library.resource.Resource;
import by.epam.library.service.impl.book.SearchServiceImpl;
import by.epam.library.service.book.SearchService;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Даша on 21.11.2015.
 */
public class SearchCommand implements Command {
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String FORWARD_PAGE_SEARCH = "forward.page.search";

    private static final String PARAM_SEARCH_QUERY = "searchQuery";
    private static final String PARAM_BOOKS = "books";
    private static final String PARAM_PAGE = "page";
    private static final String PARAM_CURRENT_PAGE = "currentPage";
    private static final String PARAM_PAGES_NUMBER = "pagesNumber";

    public String execute(HttpServletRequest request) throws CommandException {
        String query = request.getParameter(PARAM_SEARCH_QUERY);
        if (query.isEmpty()) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
        SearchService service = SearchServiceImpl.getInstance();
        int page = 1;
        String parameterPage = request.getParameter(PARAM_PAGE);
        boolean isNumber = Validator.validateNumber(parameterPage);
        if (isNumber) {
            page = Integer.parseInt(parameterPage);
        }
        List<BookVO> books = service.search(query, page);
        int pagesNumber = service.getPagesNumber(query);
        request.setAttribute(PARAM_BOOKS, books);
        request.setAttribute(PARAM_SEARCH_QUERY, query);
        request.setAttribute(PARAM_CURRENT_PAGE, page);
        request.setAttribute(PARAM_PAGES_NUMBER, pagesNumber);
        return Resource.getValue(FORWARD_PAGE_SEARCH);
    }
}
