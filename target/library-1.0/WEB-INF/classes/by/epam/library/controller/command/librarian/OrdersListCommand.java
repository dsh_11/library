package by.epam.library.controller.command.librarian;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.User;
import by.epam.library.domain.vo.LibrarianOrderVO;
import by.epam.library.resource.Resource;
import by.epam.library.service.impl.librarian.OrdersListServiceImpl;
import by.epam.library.service.librarian.OrdersListService;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Даша on 15.11.2015.
 */
public class OrdersListCommand implements Command {
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String FORWARD_PAGE_LOG_IN = "forward.page.log.in";
    private static final String FORWARD_PAGE_ORDERS = "forward.page.orders";

    private static final String PARAM_USER = "user";
    private static final String PARAM_QUERY = "query";
    private static final String PARAM_PAGE = "page";
    private static final String PARAM_STATUS = "status";
    private static final String PARAM_CURRENT_PAGE = "currentPage";
    private static final String PARAM_ORDERS = "orders";
    private static final String PARAM_PAGES_NUMBER = "pagesNumber";
    private static final String ROLE = "librarian";

    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        User currentUser = (User)session.getAttribute(PARAM_USER);
        if (currentUser == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        if (!ROLE.equals(currentUser.getRole())) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        StringBuffer query = request.getRequestURL();
        query.append('?');
        query.append(request.getQueryString());
        request.getSession(true).setAttribute(PARAM_QUERY, query);

        OrdersListService service = OrdersListServiceImpl.getInstance();
        int page = 1;
        String parameterPage = request.getParameter(PARAM_PAGE);
        boolean isNumber = Validator.validateNumber(parameterPage);
        if (isNumber) {
            page = Integer.parseInt(parameterPage);
        }
        String status = request.getParameter(PARAM_STATUS);
        boolean correct = Validator.validateString(status);
        if (!correct) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
        List<LibrarianOrderVO> orders = service.getOrders(page, status);
        int pagesNumber = service.getPagesNumber(status);
        request.setAttribute(PARAM_ORDERS, orders);
        request.setAttribute(PARAM_STATUS, status);
        request.setAttribute(PARAM_CURRENT_PAGE, page);
        request.setAttribute(PARAM_PAGES_NUMBER, pagesNumber);
        return Resource.getValue(FORWARD_PAGE_ORDERS);
    }
}
