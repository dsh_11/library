package by.epam.library.controller;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.controller.command.CommandHelper;
import by.epam.library.dao.DaoException;
import by.epam.library.resource.Resource;
import by.epam.library.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;

/**
 * Created by ���� on 03.11.2015.
 */
public class LibraryController extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(LibraryController.class);

    private static final String PARAM_COMMAND = "command";
    private static final String DEFAULT_COMMAND = "covers";
    private static final String LANG_COMMAND = "change_lang";

    private static final String FORWARD_PAGE_GENERAL_EXCEPTION = "forward.page.general.exception";

    private CommandHelper commandHelper = new CommandHelper();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String commandName = request.getParameter(PARAM_COMMAND);
        if (commandName == null) {
            commandName = DEFAULT_COMMAND;
        }
        Command command = commandHelper.getCommand(commandName);
        try {
            String forwardPage = command.execute(request);
            if (commandName.equals(LANG_COMMAND)) {
                response.sendRedirect(forwardPage);
            } else {
                request.getRequestDispatcher(forwardPage).forward(request, response);
            }
        } catch (DaoException e) {
            e.printStackTrace();
            logger.error(e);
            request.getRequestDispatcher(Resource.getValue(FORWARD_PAGE_GENERAL_EXCEPTION)).forward(request, response);
        } catch (ServiceException e) {
            e.printStackTrace();
            logger.error(e);
            request.getRequestDispatcher(Resource.getValue(FORWARD_PAGE_GENERAL_EXCEPTION)).forward(request, response);
        } catch (CommandException e) {
            e.printStackTrace();
            logger.error(e);
            request.getRequestDispatcher(Resource.getValue(FORWARD_PAGE_GENERAL_EXCEPTION)).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
            request.getRequestDispatcher(Resource.getValue(FORWARD_PAGE_GENERAL_EXCEPTION)).forward(request, response);
        }
    }
}