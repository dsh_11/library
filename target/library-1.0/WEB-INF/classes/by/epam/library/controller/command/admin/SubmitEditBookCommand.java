package by.epam.library.controller.command.admin;

import by.epam.library.controller.LibraryController;
import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.Book;
import by.epam.library.domain.User;
import by.epam.library.resource.Resource;
import by.epam.library.service.admin.EditBookService;
import by.epam.library.service.impl.admin.EditBookServiceImpl;
import by.epam.library.util.Validator;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.List;

/**
 * Created by Даша on 28.11.2015.
 */
public class SubmitEditBookCommand implements Command {
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String FORWARD_PAGE_LOG_IN = "forward.page.log.in";
    private static final String FORWARD_PAGE_CATALOGUE_COMMAND = "forward.page.catalogue.command";
    private static final String FORWARD_PAGE_EDIT_BOOK_COMMAND = "forward.page.edit.book.command";

    private static final String COVER_DIRECTORY = "images/";
    private static final String DELETED_PATH = "WEB-INF/classes/";

    private static final String PARAM_BOOK_ID = "bookId";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_AUTHOR = "author";
    private static final String PARAM_PAGES_NUMBER = "pagesNumber";
    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_USER = "user";
    private static final String PARAM_WRONG_DATA = "wrongData";
    private static final String PARAM_CURRENT_COVER = "currentCover";
    private static final String ROLE = "admin";

    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        User currentUser = (User)session.getAttribute(PARAM_USER);
        if (currentUser == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        if (!ROLE.equals(currentUser.getRole())) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        int id;
        String parameterId = request.getParameter(PARAM_BOOK_ID);
        boolean isNumber = Validator.validateNumber(parameterId);
        if (isNumber) {
            id = Integer.valueOf(parameterId);
        } else {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        if (ServletFileUpload.isMultipartContent(request)) {
            Book book = processRequest(request);
            if (book != null) {
                book.setBookId(id);
                EditBookService service = EditBookServiceImpl.getInstance();
                service.editBook(book);
                return Resource.getValue(FORWARD_PAGE_CATALOGUE_COMMAND);
            } else {
                request.setAttribute(PARAM_WRONG_DATA, true);
                request.setAttribute(PARAM_BOOK_ID, id);
                return Resource.getValue(FORWARD_PAGE_EDIT_BOOK_COMMAND);
            }
        }
        return "";
    }

    private Book processRequest(HttpServletRequest request) throws CommandException {
        String paramCover = null;
        String paramName = null;
        String paramAuthor = null;
        String paramPagesNumber = null;
        String paramDescription = null;
        String paramCurrentCover = null;
        try {
            List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

            for (FileItem item : multiparts) {
                if (!item.isFormField()) {
                    String name = item.getName();
                    if (!name.isEmpty()) {
                        String path = LibraryController.class.getClassLoader().getResource("").getPath();
                        if (path != null) {
                            path = path.replace("%20", " ").replace(DELETED_PATH, "").concat(COVER_DIRECTORY + name);
                            File file = new File(path);
                            item.write(file);
                            paramCover = COVER_DIRECTORY + name;
                        }
                    }
                } else {
                    switch (item.getFieldName()) {
                        case PARAM_NAME:
                            paramName = item.getString("utf-8");
                            break;
                        case PARAM_AUTHOR:
                            paramAuthor = item.getString("utf-8");
                            break;
                        case PARAM_PAGES_NUMBER:
                            paramPagesNumber = item.getString();
                            break;
                        case PARAM_DESCRIPTION:
                            paramDescription = item.getString("utf-8");
                            break;
                        case PARAM_CURRENT_COVER:
                            paramCurrentCover = item.getString();
                            break;
                    }
                }
            }
            Book book = null;
            boolean correct = Validator.validateEditBook(paramName, paramAuthor, paramPagesNumber, paramCurrentCover);
            if (correct) {
                book = new Book();
                if (paramCover == null) {
                    book.setCover(paramCurrentCover);
                } else {
                    book.setCover(paramCover);
                }
                book.setName(paramName);
                book.setAuthor(paramAuthor);
                book.setPagesNumber(Integer.valueOf(paramPagesNumber));
                book.setDescription(paramDescription);
            }
            return book;
        } catch (Exception e) {
            throw new CommandException("", e);
        }
    }
}