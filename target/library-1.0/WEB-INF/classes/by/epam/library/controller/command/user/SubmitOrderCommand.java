package by.epam.library.controller.command.user;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.Order;
import by.epam.library.domain.User;
import by.epam.library.resource.Resource;
import by.epam.library.service.impl.user.SubmitOrderServiceImpl;
import by.epam.library.service.user.SubmitOrderService;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Даша on 14.11.2015.
 */
public class SubmitOrderCommand implements Command {
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String FORWARD_PAGE_LOG_IN = "forward.page.log.in";
    private static final String FORWARD_PAGE_ORDER_BOOK_COMMAND = "forward.page.order.book.command";

    private static final String PARAM_USER = "user";
    private static final String PARAM_BOOK_ID = "bookId";
    private static final String PARAM_BOOK_NAME = "bookName";
    private static final String PARAM_ORDER_TYPE = "orderType";
    private static final String PARAM_NO_COPIES = "noCopies";
    private static final String PARAM_USER_HAS_ORDER ="userHasOrder";
    private static final String ROLE = "user";
    private static final String STATUS = "unconfirmed";

    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        User currentUser = (User)session.getAttribute(PARAM_USER);
        if (currentUser == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        if (!ROLE.equals(currentUser.getRole())) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        String parameterBookId = request.getParameter(PARAM_BOOK_ID);
        boolean isNumber = Validator.validateNumber(parameterBookId);
        if (!isNumber) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
        int bookId = Integer.valueOf(parameterBookId);
        int userId = currentUser.getUserId();
        String type = request.getParameter(PARAM_ORDER_TYPE);
        boolean correct = Validator.validateString(type);
        if (!correct) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
        Order order = new Order();
        order.setBookId(bookId);
        order.setUserId(userId);
        order.setStatus(STATUS);
        order.setType(type);
        SubmitOrderService service = SubmitOrderServiceImpl.getInstance();
        boolean userHasOrder = service.userHasOrder(order);
        if (userHasOrder) {
            request.setAttribute(PARAM_USER_HAS_ORDER, true);
            return Resource.getValue(FORWARD_PAGE_ORDER_BOOK_COMMAND);
        }
        boolean copiesAvailable = service.submitOrder(order);
        if (copiesAvailable) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        } else {
            String bookName = request.getParameter(PARAM_BOOK_NAME);
            request.setAttribute(PARAM_BOOK_NAME, bookName);
            request.setAttribute(PARAM_NO_COPIES, true);
            request.setAttribute(PARAM_BOOK_ID, bookId);
            return Resource.getValue(FORWARD_PAGE_ORDER_BOOK_COMMAND);
        }
    }
}
