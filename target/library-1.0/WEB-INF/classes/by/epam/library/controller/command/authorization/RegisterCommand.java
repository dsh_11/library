package by.epam.library.controller.command.authorization;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.User;
import by.epam.library.resource.Resource;
import by.epam.library.service.authorization.RegisterService;
import by.epam.library.service.impl.authorization.RegisterServiceImpl;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ���� on 03.11.2015.
 */
public class RegisterCommand implements Command {
    private static final String FORWARD_PAGE_REGISTER = "forward.page.register";
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";

    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_CONFIRM_PASSWORD = "confirmPassword";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_FIRST_NAME = "firstName";
    private static final String PARAM_LAST_NAME = "lastName";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_ROLE = "role";
    private static final String PARAM_LOGIN_EXISTS = "loginExists";
    private static final String PARAM_WRONG_DATA = "wrongData";
    private static final String PARAM_USER = "user";

    public String execute(HttpServletRequest request) throws CommandException {
        User currentUser = (User)request.getSession().getAttribute(PARAM_USER);
        if (currentUser != null) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        RegisterService service = RegisterServiceImpl.getInstance();
        String login = request.getParameter(PARAM_LOGIN);
        String password = request.getParameter(PARAM_PASSWORD);
        String firstName = request.getParameter(PARAM_FIRST_NAME);
        String lastName = request.getParameter(PARAM_LAST_NAME);
        String email = request.getParameter(PARAM_EMAIL);
        String role = request.getParameter(PARAM_ROLE);
        String confirmPassword = request.getParameter(PARAM_CONFIRM_PASSWORD);
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setRole(role);
        boolean correct = Validator.validateRegister(user, confirmPassword);
        if (!correct) {
            request.setAttribute(PARAM_WRONG_DATA, true);
            return Resource.getValue(FORWARD_PAGE_REGISTER);
        }
        boolean loginExists = service.register(user);
        if (!loginExists) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        } else {
            request.setAttribute(PARAM_LOGIN_EXISTS, true);
            return Resource.getValue(FORWARD_PAGE_REGISTER);
        }
    }
}
