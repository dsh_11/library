package by.epam.library.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by Даша on 18.11.2015.
 */
public class RegisterFormTag extends TagSupport {
    private String header;
    private String phFirstName;
    private String phLastName;
    private String phEmail;
    private String phLogin;
    private String phPassword;
    private String phPasswordConfirm;

    public int doStartTag() throws JspException {
        try {
            pageContext.getOut().write("<h2 class=\"form-register-heading\">" + header + "</h2>");
            pageContext.getOut().write("<div id=\"first-name\">\n" +
                    "          <input class=\"form-control\" type=\"text\" name=\"firstName\" id=\"firstNameInput\" " +
                    "autocomplete=\"off\" placeholder=\"" + phFirstName + "\" />\n" +
                    "        </div>");
            pageContext.getOut().write("<div id=\"last-name\">\n" +
                    "          <input class=\"form-control\" type=\"text\" name=\"lastName\" id=\"lastNameInput\" " +
                    "autocomplete=\"off\" placeholder=\"" + phLastName + "\" />\n" +
                    "        </div>");
            pageContext.getOut().write("<div id=\"email\">\n" +
                    "          <input class=\"form-control\" type=\"text\" name=\"email\" id=\"emailInput\" " +
                    "autocomplete=\"off\" placeholder=\"" + phEmail + "\" />\n" +
                    "        </div>");
            pageContext.getOut().write("<div id=\"login\">\n" +
                    "          <input class=\"form-control\" type=\"text\" name=\"login\" id=\"loginInput\" " +
                    "autocomplete=\"off\" placeholder=\"" + phLogin + "\" />\n" +
                    "        </div>");
            pageContext.getOut().write("<div id=\"password\">\n" +
                    "          <input class=\"form-control\" type=\"password\" name=\"password\" id=\"passwordInput\" " +
                    "placeholder=\"" + phPassword + "\" />\n" +
                    "        </div>");
            pageContext.getOut().write("<div id=\"confirm-pass\">\n" +
                    "          <input class=\"form-control\" type=\"password\" name=\"confirmPassword\" id=\"confirmPassInput\" " +
                    "placeholder=\"" + phPasswordConfirm + "\" />\n" +
                    "        </div>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getPhFirstName() {
        return phFirstName;
    }

    public void setPhFirstName(String phFirstName) {
        this.phFirstName = phFirstName;
    }

    public String getPhLastName() {
        return phLastName;
    }

    public void setPhLastName(String phLastName) {
        this.phLastName = phLastName;
    }

    public String getPhLogin() {
        return phLogin;
    }

    public void setPhLogin(String phLogin) {
        this.phLogin = phLogin;
    }

    public String getPhEmail() {
        return phEmail;
    }

    public void setPhEmail(String phEmail) {
        this.phEmail = phEmail;
    }

    public String getPhPassword() {
        return phPassword;
    }

    public void setPhPassword(String phPassword) {
        this.phPassword = phPassword;
    }

    public String getPhPasswordConfirm() {
        return phPasswordConfirm;
    }

    public void setPhPasswordConfirm(String phPasswordConfirm) {
        this.phPasswordConfirm = phPasswordConfirm;
    }
}
