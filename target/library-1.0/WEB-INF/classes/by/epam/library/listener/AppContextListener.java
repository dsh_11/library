package by.epam.library.listener;

import by.epam.library.dao.connectionpool.ConnectionPoolImpl;
import by.epam.library.dao.connectionpool.ConnectionPoolException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Locale;

/**
 * Created by Даша on 10.11.2015.
 */
public class AppContextListener implements ServletContextListener {
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        Locale.setDefault(Locale.ENGLISH);
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        try {
            connectionPool.initialize();
        } catch (ConnectionPoolException e) {
            e.printStackTrace();
        }
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ConnectionPoolImpl connectionPool = ConnectionPoolImpl.getInstance();
        connectionPool.dispose();
    }
}
