function validateRegEx(regex, input) {
    if (!regex.test(input)) {
        return false;
    }
    else {
        return true;
    }
}

function validateEmpty(field) {
    return (field.value.length == 0);
}

function validateName(name, output) {
    if (validateEmpty(name)) {
        output.style.display = "block";
        return false;
    }
    if (!validateRegEx(/^[A-Za-zА-Яа-я]+$/, name.value)) {
        output.style.display = "block";
        return false;
    }
    output.style.display = "none";
    return true;
}

function validateEmail(email, output) {
    if (!validateRegEx(/^[\w\.-_\+]+@[\w-]+(\.\w{2,3})+$/, email.value)) {
        output.style.display = "block";
        return false;
    }
    output.style.display = "none";
    return true;
}

function validateLogin(login, output) {
    if (validateEmpty(login)) {
        output.style.display = "block";
        return false;
    }
    output.style.display = "none";
    return true;
}

function validatePassword(password, output) {
    if (!((password.value.length >= 6) && (password.value.length <= 32))) {
        output.style.display = "block";
        return false;
    }
    output.style.display = "none";
    return true;
}

function validatePasswordsMatch(pass1, pass2, output) {
    if ((pass1.value.length >= 6) && (pass1.value.length <= 32)) {
        if (pass1.value == pass2.value) {
            output.style.display = "none";
            return true;
        } else {
            output.style.display = "block";
            return false;
        }
    } else if (pass2.value.length > 0) {
        output.style.display = "block";
        return false;
    } else {
        output.style.display = "none";
        return false;
    }
}

function validateForm(form) {
    var passwordValid = validatePassword(form["passwordInput"], document.getElementById("passwordMessage"));
    var passwordsMatch = validatePasswordsMatch(form["confirmPassInput"], form["passwordInput"],
        document.getElementById("confirmPassMessage"));
    var loginValid = validateLogin(form["loginInput"], document.getElementById("loginMessage"));
    var emailValid = validateEmail(form["emailInput"], document.getElementById("emailMessage"));
    var firstNameValid = validateName(form["firstNameInput"], document.getElementById("firstNameMessage"));
    var lastNameValid = validateName(form["lastNameInput"], document.getElementById("lastNameMessage"));
    return passwordValid && passwordsMatch && loginValid && emailValid && firstNameValid && lastNameValid;
}

function submitForm(form) {
    var correct = validateForm(form);
    if (correct) {
        form.submit();
    } /*else {
        if (!passwordValid) {
            error("password");
        }
        if (!passwordsMatch) {
            error("confirm-pass");
        }
        if (!loginValid) {
            error("login");
        }
        if (!emailValid) {
            error("email");
        }
        if (!firstNameValid) {
            error("first-name");
        }
        if (!lastNameValid) {
            error("last-name");
        }
    }*/
}

function submitFormLogIn(form) {
    var passwordValid = validatePassword(form["passwordInput"], document.getElementById("passwordMessage"));
    var loginValid = validateLogin(form["loginInput"], document.getElementById("loginMessage"));
    if (passwordValid && loginValid) {
        form.submit();
    }
}

function submitAddUser(form) {
    var correct = validateForm(form);
    if (correct) {
        var value;
        var radios = form.elements["optradio"];

        for (var i = 0, len = radios.length; i < len; i++) {
            if (radios[i].checked) {
                value = radios[i].value;
                break;
            }
        }
        document.getElementsByName("role")[0].value = value;
        form.submit();
    }
}

function submitSearchForm(form) {
    var correct = !validateEmpty(document.getElementsByName("searchQuery")[0]);
    if (correct) {
        form.submit();
    }
}

function submitAddBook(form) {
    var nameValid = form["nameInput"].value.length > 0;
    var authorValid = form["authorInput"].value.length > 0;
    var pagesNumberValid = validateRegEx(/^\d+$/, form["pagesNumberInput"].value);
    var isbnValid = form["isbnInput"].value.length > 0;
    var amountValid = validateRegEx(/^\d+$/, form["amountInput"].value);
    if (nameValid && authorValid && pagesNumberValid && isbnValid && amountValid) {
        form.submit();
    }
    if (!nameValid) {
        error("#nameInput");
        document.getElementById("nameMessage").style.display = "block";
    }
    if (!authorValid) {
        error("#authorInput");
        document.getElementById("authorMessage").style.display = "block";
    }
    if (!pagesNumberValid) {
        error("#pagesNumberInput");
        document.getElementById("pagesNumberMessage").style.display = "block";
    }
    if (!isbnValid) {
        error("#isbnInput");
        document.getElementById("isbnMessage").style.display = "block";
    }
    if (!amountValid) {
        error("#amountInput");
        document.getElementById("amountMessage").style.display = "block";
    }
}

function submitEditBook(form) {
    var nameValid = form["nameInput"].value.length > 0;
    var authorValid = form["authorInput"].value.length > 0;
    var pagesNumberValid = validateRegEx(/^\d+$/, form["pagesNumberInput"].value);
    if (nameValid && authorValid && pagesNumberValid) {
        form.submit();
    }
    if (!nameValid) {
        error("#nameInput");
        document.getElementById("nameMessage").style.display = "block";
    }
    if (!authorValid) {
        error("#authorInput");
        document.getElementById("authorMessage").style.display = "block";
    }
    if (!pagesNumberValid) {
        error("#pagesNumberInput");
        document.getElementById("pagesNumberMessage").style.display = "block";
    }
}

function success(idName){
    $(idName).removeClass('has-error');
    $(idName).addClass('has-success');
}

function error(idName) {
    $(idName).removeClass('has-success');
    $(idName).addClass('has-error');
}

$(function() {
   $("#first-name input").blur(function(e) {
       e.preventDefault();
       if (validateName(this, document.getElementById('firstNameMessage'))) {
           success("#first-name");
       } else {
           error("#first-name");
       }
   });
});

$(function() {
    $("#last-name input").blur(function(e) {
        e.preventDefault();
        if (validateName(this, document.getElementById('lastNameMessage'))) {
            success("#last-name");
        } else {
            error("#last-name");
        }
    });
});

$(function() {
    $("#email input").blur(function(e) {
        e.preventDefault();
        if (validateEmail(this, document.getElementById('emailMessage'))) {
            success("#email");
        } else {
            error("#email");
        }
    });
});

$(function() {
    $("#login input").blur(function(e) {
        e.preventDefault();
        if (validateLogin(this, document.getElementById('loginMessage'))) {
            success("#login");
        } else {
            error("#login");
        }
    });
});

$(function() {
    $("#password input").blur(function(e) {
        e.preventDefault();
        if (validatePassword(this, document.getElementById('passwordMessage'))) {
            success("#password");
        } else {
            error("#password");
        }
    });
});

$(function() {
   $("#confirm-pass input").blur(function(e) {
       e.preventDefault();
       if (validatePasswordsMatch(this, document.getElementById('passwordInput'), document.getElementById('confirmPassMessage'))) {
           success("#confirm-pass");
       } else {
           error("#confirm-pass");
       }
   });
});
