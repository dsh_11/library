function submitOrder(form) {
    var value;
    var radios = form.elements["optradio"];

    for (var i=0, len=radios.length; i<len; i++) {
        if (radios[i].checked) {
            value = radios[i].value;
            break;
        }
    }
    document.getElementsByName("orderType")[0].value = value;
    form.submit();
}