package by.epam.library.util;

import by.epam.library.domain.Book;
import by.epam.library.domain.User;

/**
 * Created by Даша on 17.11.2015.
 */
public class Validator {
    public static boolean validateLogIn(User user) {
        return validateLogin(user.getLogin()) && validatePassword(user.getPassword());
    }

    public static boolean validateRegister(User user, String confirmPassword) {
        return validateLogin(user.getLogin()) && validatePassword(user.getPassword()) &&
                validateName(user.getFirstName()) && validateName(user.getLastName()) &&
                validateEmail(user.getEmail()) && validatePasswordsMatch(user.getPassword(), confirmPassword);
    }

    public static boolean validateAddUser(User user) {
        return validateLogin(user.getLogin()) && validatePassword(user.getPassword()) &&
                validateName(user.getFirstName()) && validateName(user.getLastName()) &&
                validateEmail(user.getEmail()) && validateRole(user.getRole());
    }

    public static boolean validateAddBook(String name, String author, String pagesNumber, String isbn, String amount) {
        return validateString(name) && validateString(author) && validateString(isbn) &&
                validateNumber(pagesNumber) && validateNumber(amount);
    }

    public static boolean validateEditBook(String name, String author, String pagesNumber, String currentCover) {
        return validateString(name) && validateString(author) && validateNumber(pagesNumber) &&
                validateString(currentCover);
    }

    public static boolean validateNumber(String number) {
        return validateString(number) && number.matches("^\\d+$");
    }

    public static boolean validateString(String field) {
        return (field != null) && !(field.isEmpty());
    }

    private static boolean validateLogin(String login) {
        return !(login == null) && !login.isEmpty();
    }

    private static boolean validatePassword(String password) {
        return !(password == null) && (password.length() >= 6) && (password.length() <= 32);
    }

    private static boolean validateName(String name) {
        return !(name == null) && name.matches("^[A-Za-zА-Яа-я]+$");
    }

    private static boolean validateEmail(String email) {
        return !(email == null) && email.matches("^[\\w\\.-_\\+]+@[\\w-]+(\\.\\w{2,3})+$");
    }

    private static boolean validatePasswordsMatch(String pass1, String pass2) {
        return pass1.equals(pass2);
    }

    private static boolean validateRole(String role) {
        return !(role == null);
    }
}