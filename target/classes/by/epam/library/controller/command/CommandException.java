package by.epam.library.controller.command;

/**
 * Created by ���� on 03.11.2015.
 */
public class CommandException extends Exception {
    private static final long serialVersionUID = 1L;

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Exception e) {
        super(message, e);
    }
}
