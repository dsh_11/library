package by.epam.library.controller.command.book;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.vo.BookVO;
import by.epam.library.resource.Resource;
import by.epam.library.service.book.CatalogueService;
import by.epam.library.service.impl.book.CatalogueServiceImpl;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Даша on 21.11.2015.
 */
public class CatalogueCommand implements Command {
    private static final String FORWARD_PAGE_CATALOGUE = "forward.page.catalogue";

    private static final String PARAM_QUERY = "query";
    private static final String PARAM_PAGE = "page";
    private static final String PARAM_BOOKS = "books";
    private static final String PARAM_CURRENT_PAGE = "currentPage";
    private static final String PARAM_PAGES_NUMBER = "pagesNumber";

    public String execute(HttpServletRequest request) throws CommandException {
        StringBuffer query = request.getRequestURL();
        query.append('?');
        query.append(request.getQueryString());
        request.getSession(true).setAttribute(PARAM_QUERY, query);

        CatalogueService service = CatalogueServiceImpl.getInstance();
        int page = 1;
        String parameterPage = request.getParameter(PARAM_PAGE);
        boolean isNumber = Validator.validateNumber(parameterPage);
        if (isNumber) {
            page = Integer.parseInt(parameterPage);
        }
        List<BookVO> books = service.getBooks(page);
        int pagesNumber = service.getPagesNumber();
        request.setAttribute(PARAM_BOOKS, books);
        request.setAttribute(PARAM_CURRENT_PAGE, page);
        request.setAttribute(PARAM_PAGES_NUMBER, pagesNumber);
        return Resource.getValue(FORWARD_PAGE_CATALOGUE);
    }
}
