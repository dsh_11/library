package by.epam.library.controller.command.book;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.Book;
import by.epam.library.resource.Resource;
import by.epam.library.service.book.BookInfoService;
import by.epam.library.service.impl.book.BookInfoServiceImpl;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Даша on 12.11.2015.
 */
public class BookInfoCommand implements Command {
    private static final String FORWARD_PAGE_BOOK_INFO = "forward.page.book.info";
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";

    private static final String PARAM_QUERY = "query";
    private static final String PARAM_BOOK_ID = "bookId";
    private static final String PARAM_BOOK = "book";

    public String execute(HttpServletRequest request) throws CommandException {
        StringBuffer query = request.getRequestURL();
        query.append('?');
        query.append(request.getQueryString());
        request.getSession(true).setAttribute(PARAM_QUERY, query);

        BookInfoService service = BookInfoServiceImpl.getInstance();
        int id;
        String parameterId = request.getParameter(PARAM_BOOK_ID);
        boolean isNumber = Validator.validateNumber(parameterId);
        if (isNumber) {
            id = Integer.valueOf(parameterId);
        } else {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
        Book book = service.getBook(id);
        if (book != null) {
            request.setAttribute(PARAM_BOOK, book);
            return Resource.getValue(FORWARD_PAGE_BOOK_INFO);
        } else {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
    }
}