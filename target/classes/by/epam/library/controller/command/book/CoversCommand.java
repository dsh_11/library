package by.epam.library.controller.command.book;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.resource.Resource;
import by.epam.library.service.book.CoversService;
import by.epam.library.service.impl.book.CoversServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by Даша on 07.11.2015.
 */
public class CoversCommand implements Command {
    private static final String FORWARD_PAGE_HOME = "forward.page.home";

    private static final String PARAM_QUERY = "query";
    private static final String PARAM_COVERS = "covers";

    public String execute(HttpServletRequest request) throws CommandException {
        StringBuffer query = request.getRequestURL();
        query.append('?');
        query.append(request.getQueryString());
        request.getSession(true).setAttribute(PARAM_QUERY, query);

        CoversService service = CoversServiceImpl.getInstance();
        Map<Integer, String> booksCovers = service.getBooksCovers();
        request.setAttribute(PARAM_COVERS, booksCovers);
        return Resource.getValue(FORWARD_PAGE_HOME);
    }
}
