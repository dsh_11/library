package by.epam.library.controller.command.admin;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.User;
import by.epam.library.resource.Resource;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Даша on 29.11.2015.
 */
public class AddCopiesCommand implements Command {
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String FORWARD_PAGE_LOG_IN = "forward.page.log.in";
    private static final String FORWARD_PAGE_ADD_COPIES = "forward.page.add.copies";

    private static final String PARAM_BOOK_ID = "bookId";
    private static final String PARAM_BOOK_NAME = "bookName";
    private static final String PARAM_USER = "user";
    private static final String PARAM_WRONG_DATA = "wrongData";
    private static final String ROLE = "admin";

    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        User currentUser = (User)session.getAttribute(PARAM_USER);
        if (currentUser == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        if (!ROLE.equals(currentUser.getRole())) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        int id;
        String parameterId = request.getParameter(PARAM_BOOK_ID);
        boolean isNumber = Validator.validateNumber(parameterId);
        if (isNumber) {
            id = Integer.valueOf(parameterId);
        } else {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
        Object wrongData = request.getAttribute(PARAM_WRONG_DATA);
        request.setAttribute(PARAM_WRONG_DATA, wrongData);
        String parameterName = request.getParameter(PARAM_BOOK_NAME);
        request.setAttribute(PARAM_BOOK_ID, id);
        request.setAttribute(PARAM_BOOK_NAME, parameterName);
        return Resource.getValue(FORWARD_PAGE_ADD_COPIES);
    }
}