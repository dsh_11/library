package by.epam.library.controller.command.admin;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.User;
import by.epam.library.resource.Resource;
import by.epam.library.service.admin.AddUserService;
import by.epam.library.service.impl.admin.AddUserServiceImpl;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Даша on 12.11.2015.
 */
public class AddUserCommand implements Command {
    private static final String FORWARD_PAGE_USERS_COMMAND = "forward.page.users.command";
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String FORWARD_PAGE_LOG_IN = "forward.page.log.in";
    private static final String FORWARD_PAGE_ADD_USER = "forward.page.add.user";

    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_FIRST_NAME = "firstName";
    private static final String PARAM_LAST_NAME = "lastName";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_ROLE = "role";
    private static final String PARAM_LOGIN_EXISTS = "loginExists";
    private static final String PARAM_WRONG_DATA = "wrongData";
    private static final String PARAM_USER = "user";
    private static final String ROLE = "admin";

    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        User currentUser = (User)session.getAttribute(PARAM_USER);
        if (currentUser == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        if (!ROLE.equals(currentUser.getRole())) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
        AddUserService service = AddUserServiceImpl.getInstance();
        String login = request.getParameter(PARAM_LOGIN);
        String password = request.getParameter(PARAM_PASSWORD);
        String firstName = request.getParameter(PARAM_FIRST_NAME);
        String lastName = request.getParameter(PARAM_LAST_NAME);
        String email = request.getParameter(PARAM_EMAIL);
        String role = request.getParameter(PARAM_ROLE);
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setRole(role);
        boolean correct = Validator.validateAddUser(user);
        if (!correct) {
            request.setAttribute(PARAM_WRONG_DATA, true);
            return Resource.getValue(FORWARD_PAGE_ADD_USER);
        }
        correct =  service.addUser(user);
        if (correct) {
            return Resource.getValue(FORWARD_PAGE_USERS_COMMAND);
        } else {
            request.setAttribute(PARAM_LOGIN_EXISTS, true);
            return Resource.getValue(FORWARD_PAGE_ADD_USER);
        }
    }
}
