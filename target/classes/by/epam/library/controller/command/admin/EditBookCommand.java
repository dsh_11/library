package by.epam.library.controller.command.admin;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.Book;
import by.epam.library.domain.User;
import by.epam.library.resource.Resource;
import by.epam.library.service.admin.EditBookService;
import by.epam.library.service.impl.admin.EditBookServiceImpl;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Даша on 27.11.2015.
 */
public class EditBookCommand implements Command {
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String FORWARD_PAGE_LOG_IN = "forward.page.log.in";
    private static final String FORWARD_PAGE_EDIT_BOOK = "/WEB-INF/jsp/admin/edit_book.jsp";

    private static final String PARAM_BOOK_ID = "bookId";
    private static final String PARAM_WRONG_DATA = "wrongData";
    private static final String PARAM_USER = "user";
    private static final String PARAM_BOOK = "book";
    private static final String ROLE = "admin";

    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        User currentUser = (User)session.getAttribute(PARAM_USER);
        if (currentUser == null) {
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        if (!ROLE.equals(currentUser.getRole())) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        int id;
        String parameterId = request.getParameter(PARAM_BOOK_ID);
        boolean isNumber = Validator.validateNumber(parameterId);
        if (isNumber) {
            id = Integer.valueOf(parameterId);
        } else {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        EditBookService service = EditBookServiceImpl.getInstance();
        Book book = service.getBook(id);
        if (book != null) {
            Object wrongData = request.getAttribute(PARAM_WRONG_DATA);
            request.setAttribute(PARAM_WRONG_DATA, wrongData);
            request.setAttribute(PARAM_BOOK, book);
            return FORWARD_PAGE_EDIT_BOOK;
        } else {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
    }
}
