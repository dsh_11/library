package by.epam.library.controller.command.authorization;

import by.epam.library.controller.command.Command;
import by.epam.library.controller.command.CommandException;
import by.epam.library.domain.User;
import by.epam.library.resource.Resource;
import by.epam.library.service.authorization.LogInService;
import by.epam.library.service.impl.authorization.LogInServiceImpl;
import by.epam.library.util.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by ���� on 03.11.2015.
 */
public class LogInCommand implements Command {
    private static final String FORWARD_PAGE_HOME_COMMAND = "forward.page.home.command";
    private static final String FORWARD_PAGE_LOG_IN = "forward.page.log.in";

    private static final String PARAM_USER = "user";
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_WRONG_DATA = "wrongData";

    public String execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }
        User currentUser = (User)session.getAttribute(PARAM_USER);
        if (currentUser != null) {
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        }

        LogInService service = LogInServiceImpl.getInstance();
        String login = request.getParameter(PARAM_LOGIN);
        String password = request.getParameter(PARAM_PASSWORD);
        User userData = new User();
        userData.setLogin(login);
        userData.setPassword(password);
        boolean correct = Validator.validateLogIn(userData);
        if (!correct) {
            request.setAttribute(PARAM_WRONG_DATA, true);
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
        User user = service.logIn(userData);
        if (user != null) {
            request.getSession(true).setAttribute(PARAM_USER, user);
            return Resource.getValue(FORWARD_PAGE_HOME_COMMAND);
        } else {
            request.setAttribute(PARAM_WRONG_DATA, true);
            return Resource.getValue(FORWARD_PAGE_LOG_IN);
        }
    }
}
