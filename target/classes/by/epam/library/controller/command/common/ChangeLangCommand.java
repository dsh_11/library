package by.epam.library.controller.command.common;

import by.epam.library.controller.command.Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by ���� on 05.11.2015.
 */
public class ChangeLangCommand implements Command {
    private static final String PARAM_LANGUAGE = "language";
    private static final String PARAM_QUERY = "query";
    private static final String LANG_RU = "ru";
    private static final String LANG_EN = "en";

    public String execute(HttpServletRequest request) {
        String currentLang = (String)request.getSession(true).getAttribute(PARAM_LANGUAGE);
        if (LANG_EN.equals(currentLang)) {
            currentLang = LANG_RU;
        } else {
            currentLang = LANG_EN;
        }
        request.getSession(true).setAttribute(PARAM_LANGUAGE, currentLang);
        String forwardPage = request.getSession(true).getAttribute(PARAM_QUERY).toString();
        return forwardPage;
    }
}