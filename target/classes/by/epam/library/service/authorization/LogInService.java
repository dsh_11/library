package by.epam.library.service.authorization;

import by.epam.library.domain.User;
import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 15.11.2015.
 */
public interface LogInService {
    User logIn(User user) throws ServiceException;
}
