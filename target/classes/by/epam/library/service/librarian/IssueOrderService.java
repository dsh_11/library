package by.epam.library.service.librarian;

import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 15.11.2015.
 */
public interface IssueOrderService {
    void issueOrder(int orderId) throws ServiceException;
}
