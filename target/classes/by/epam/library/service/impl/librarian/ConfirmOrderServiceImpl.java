package by.epam.library.service.impl.librarian;

import by.epam.library.dao.OrderDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.service.ServiceException;
import by.epam.library.service.librarian.ConfirmOrderService;

/**
 * Created by Даша on 15.11.2015.
 */
public class ConfirmOrderServiceImpl implements ConfirmOrderService {
    private ConfirmOrderServiceImpl () {}

    private static class SingletonHelper {
        private static final ConfirmOrderServiceImpl instance = new ConfirmOrderServiceImpl();
    }

    public static ConfirmOrderServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public void confirmOrder(int orderId) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        OrderDao orderDao = daoFactory.getOrderDao();
        orderDao.confirmOrder(orderId);
    }
}
