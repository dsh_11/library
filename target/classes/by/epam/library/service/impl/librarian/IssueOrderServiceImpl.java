package by.epam.library.service.impl.librarian;

import by.epam.library.dao.OrderDao;
import by.epam.library.dao.StorageDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.service.ServiceException;
import by.epam.library.service.librarian.IssueOrderService;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Даша on 15.11.2015.
 */
public class IssueOrderServiceImpl implements IssueOrderService {
    private static final String ORDER_TYPE = "home";

    private IssueOrderServiceImpl() {}

    private static class SingletonHelper {
        private static final IssueOrderServiceImpl instance = new IssueOrderServiceImpl();
    }

    public static IssueOrderServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public void issueOrder(int orderId) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        OrderDao orderDao = daoFactory.getOrderDao();
        StorageDao storageDao = daoFactory.getStorageDao();
        Calendar calendar = Calendar.getInstance();
        Date issueDate = new Date(calendar.getTimeInMillis());
        Date returnDate;
        storageDao.takeBookFromStorage(orderId);
        String orderType = orderDao.getOrderType(orderId);
        if (orderType.equals(ORDER_TYPE)) {
            calendar.add(Calendar.DATE, 31);
            returnDate = new Date(calendar.getTimeInMillis());
        } else {
            returnDate = issueDate;
        }
        orderDao.issueOrder(orderId, issueDate, returnDate);
    }
}
