package by.epam.library.service.impl.admin;

import by.epam.library.dao.UserDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.User;
import by.epam.library.service.ServiceException;
import by.epam.library.service.admin.UsersListService;

import java.util.List;

/**
 * Created by Даша on 12.11.2015.
 */
public class UsersListServiceImpl implements UsersListService {
    private static final int USERS_NUMBER_TO_SHOW = 10;

    private UsersListServiceImpl() {}

    private static class SingletonHelper {
        private static final UsersListServiceImpl instance = new UsersListServiceImpl();
    }

    public static UsersListServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public List<User> getUsers(int page) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        UserDao userDao = daoFactory.getUserDao();
        int offset = (page - 1) * USERS_NUMBER_TO_SHOW;
        return userDao.getUsersList(offset, USERS_NUMBER_TO_SHOW);
    }

    public int getPagesNumber() throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        UserDao userDao = daoFactory.getUserDao();
        int usersNumber = userDao.getUsersNumber();
        int pagesNumber = (int) Math.ceil((double) usersNumber / USERS_NUMBER_TO_SHOW);
        return pagesNumber;
    }
}
