package by.epam.library.service.impl.admin;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.admin.EditBookService;

/**
 * Created by Даша on 27.11.2015.
 */
public class EditBookServiceImpl implements EditBookService {
    private EditBookServiceImpl() {}

    private static class SingletonHelper {
        private static final EditBookServiceImpl instance = new EditBookServiceImpl();
    }

    public static EditBookServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public Book getBook(int bookId) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        BookDao bookDao = daoFactory.getBookDao();
        return bookDao.getBook(bookId);
    }

    public boolean editBook(Book book) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        BookDao bookDao = daoFactory.getBookDao();
        return bookDao.editBook(book);
    }
}