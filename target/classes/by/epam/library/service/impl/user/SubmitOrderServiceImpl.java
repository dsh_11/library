package by.epam.library.service.impl.user;

import by.epam.library.dao.OrderDao;
import by.epam.library.dao.StorageDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.Order;
import by.epam.library.service.ServiceException;
import by.epam.library.service.user.SubmitOrderService;

/**
 * Created by Даша on 12.11.2015.
 */
public class SubmitOrderServiceImpl implements SubmitOrderService {
    private SubmitOrderServiceImpl() {}

    private static class SingletonHelper {
        private static final SubmitOrderServiceImpl instance = new SubmitOrderServiceImpl();
    }

    public static SubmitOrderServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public boolean submitOrder(Order order) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        OrderDao orderDao = daoFactory.getOrderDao();
        StorageDao storageDao = daoFactory.getStorageDao();
        int freeBooksNumber = storageDao.getFreeBooksNumber(order.getBookId());
        if (freeBooksNumber > 0) {
            storageDao.markBookAsOrdered(order.getBookId());
            orderDao.createOrder(order);
            return true;
        }
        return false;
    }

    public boolean userHasOrder(Order order) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        OrderDao orderDao = daoFactory.getOrderDao();
        return orderDao.userHasOrder(order);
    }
}