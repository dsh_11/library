package by.epam.library.service.impl.user;

import by.epam.library.dao.OrderDao;
import by.epam.library.dao.StorageDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.service.ServiceException;
import by.epam.library.service.user.CancelOrderService;

/**
 * Created by Даша on 19.11.2015.
 */
public class CancelOrderServiceImpl implements CancelOrderService {
    private CancelOrderServiceImpl() {}

    private static class SingletonHelper {
        private static final CancelOrderServiceImpl instance = new CancelOrderServiceImpl();
    }

    public static CancelOrderServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public void cancelOrder(int orderId, int bookId) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        StorageDao storageDao = daoFactory.getStorageDao();
        OrderDao orderDao = daoFactory.getOrderDao();
        storageDao.unmarkBookAsOrdered(bookId);
        orderDao.removeOrder(orderId);
    }
}
