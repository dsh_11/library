package by.epam.library.service.impl.book;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.factory.DaoFactory;
import by.epam.library.domain.Book;
import by.epam.library.service.ServiceException;
import by.epam.library.service.book.BookInfoService;

/**
 * Created by Даша on 12.11.2015.
 */
public class BookInfoServiceImpl implements BookInfoService {
    private BookInfoServiceImpl() {}

    private static class SingletonHelper {
        private static final BookInfoServiceImpl instance = new BookInfoServiceImpl();
    }

    public static BookInfoServiceImpl getInstance() {
        return SingletonHelper.instance;
    }

    public Book getBook(int id) throws ServiceException {
        DaoFactory daoFactory = DaoFactory.getDaoFactory(1);
        if (daoFactory == null) {
            throw new ServiceException("Wrong dao factory type");
        }
        BookDao bookDao = daoFactory.getBookDao();
        return bookDao.getBook(id);
    }
}
