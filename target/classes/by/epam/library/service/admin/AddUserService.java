package by.epam.library.service.admin;

import by.epam.library.domain.User;
import by.epam.library.service.ServiceException;

/**
 * Created by Даша on 15.11.2015.
 */
public interface AddUserService {
    boolean addUser(User user) throws ServiceException;
}
