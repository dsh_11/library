package by.epam.library.resource;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by ���� on 03.11.2015.
 */
public class Resource {
    public static String getValue(String key) {
        ResourceBundle bundle = ResourceBundle.getBundle("by.epam.library.resource.prop");
        return bundle.getString(key);
    }

    public static String getValue(Locale locale, String key) {
        ResourceBundle bundle = ResourceBundle.getBundle("by.epam.library.resource.prop", locale);
        return bundle.getString(key);
    }
}