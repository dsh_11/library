package by.epam.library.dao.impl;

import by.epam.library.dao.BookDao;
import by.epam.library.dao.DaoException;
import by.epam.library.dao.connectionpool.ConnectionPool;
import by.epam.library.dao.connectionpool.ConnectionPoolImpl;
import by.epam.library.dao.connectionpool.ConnectionPoolException;
import by.epam.library.domain.Book;
import by.epam.library.domain.vo.BookVO;
import by.epam.library.resource.Resource;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Даша on 08.11.2015.
 */
public class MySQLBookDao implements BookDao {
    private static final String SQL_GET_BOOKS_COVERS = "sql.get.books.covers";
    private static final String SQL_GET_BOOKS_NUMBER = "sql.get.books.number";
    private static final String SQL_GET_BOOK = "sql.get.book";
    private static final String SQL_SEARCH = "sql.search";
    private static final String SQL_GET_SEARCHED_BOOKS_NUMBER = "sql.get.searched.books.number";
    private static final String SQL_GET_BOOKS = "sql.get.books";
    private static final String SQL_CREATE_BOOK = "sql.create.book";
    private static final String SQL_EDIT_BOOK = "sql.edit.book";

    private MySQLBookDao() {}

    private static class SingletonHelper {
        private static final MySQLBookDao instance = new MySQLBookDao();
    }

    public static MySQLBookDao getInstance() {
        return SingletonHelper.instance;
    }

    /**
     * Returns a map of books ids and paths to covers
     * in number of <code>number</code>.
     *
     * @param number number of books to be returned
     * @return a map of books ids and paths to covers
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public Map<Integer, String> getBooksCovers(int number) throws DaoException {
        Map<Integer, String> paths = new HashMap<>();
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_BOOKS_COVERS))) {
            statement.setInt(1, number);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                paths.put(resultSet.getInt(1), resultSet.getString(2));
                //resultSet.getString(3);
            }
            return paths;
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            //System.err.println(e.toString());
            /*final Throwable[] suppressed = e.getSuppressed();
            for (final Throwable ex : suppressed) {
                System.err.println(ex);
            }*/
            throw new DaoException("Couldn't get books covers", e);
        }
    }

    /**
     * Returns total number of books in the database.
     *
     * @return total number of books in the database
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public int getBooksNumber() throws DaoException {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(Resource.getValue(SQL_GET_BOOKS_NUMBER));
            resultSet.next();
            return resultSet.getInt(1);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns book by id.
     *
     * @param id id of book to be returned
     * @return Book by id
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public Book getBook(int id) throws DaoException {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        Book book = null;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_BOOK))) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String name = resultSet.getString(1);
                String author = resultSet.getString(2);
                int pagesNumber = resultSet.getInt(3);
                String description = resultSet.getString(4);
                String isbn = resultSet.getString(5);
                String cover_path = resultSet.getString(6);
                int amount = resultSet.getInt(7);
                book = new Book();
                book.setBookId(id);
                book.setName(name);
                book.setAuthor(author);
                book.setPagesNumber(pagesNumber);
                book.setDescription(description);
                book.setIsbn(isbn);
                book.setCover(cover_path);
                book.setAmount(amount);
            }
            return book;
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns a list of books that match <code>query</code>.
     *
     * @param query query by which books are searched in the database
     * @param offset offset in the database from which books should be returned
     * @param number number of books to be returned
     * @return a list of books that match <code>query</code>
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public List<BookVO> search(String query, int offset, int number) throws DaoException {
        List<BookVO> books = new ArrayList<>();
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_SEARCH))) {
            statement.setString(1, query);
            statement.setString(2, query);
            statement.setInt(3, offset);
            statement.setInt(4, number);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int bookId = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String author = resultSet.getString(3);
                String cover = resultSet.getString(4);
                BookVO book = new BookVO();
                book.setBookId(bookId);
                book.setName(name);
                book.setAuthor(author);
                book.setCover(cover);
                books.add(book);
            }
            return books;
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns number of books that match <code>query</code>.
     *
     * @param query query by which books are searched in the database
     * @return number of books that match <code>query</code>
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public int getSearchedBooksNumber(String query) throws DaoException {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_SEARCHED_BOOKS_NUMBER))) {
            statement.setString(1, query);
            statement.setString(2, query);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Returns a list of books in number of <code>number</code>.
     *
     * @param offset offset in the database from which books should be returned
     * @param number number of books to be returned
     * @return a list of books
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public List<BookVO> getBooks(int offset, int number) throws DaoException {
        List<BookVO> books = new ArrayList<>();
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_GET_BOOKS))) {
            statement.setInt(1, offset);
            statement.setInt(2, number);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int bookId = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String author = resultSet.getString(3);
                String cover = resultSet.getString(4);
                BookVO book = new BookVO();
                book.setBookId(bookId);
                book.setName(name);
                book.setAuthor(author);
                book.setCover(cover);
                books.add(book);
            }
            return books;
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    /**
     * Creates a book <code>book</code>.
     *
     * @param book book to be created
     * @return <code>true</code> if book was created
     * @throws DaoException if ConnectionPoolException
     *         or SQLException occur
     */
    public boolean createBook(Book book) throws DaoException {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_CREATE_BOOK))) {
            statement.setString(1, book.getName());
            statement.setString(2, book.getAuthor());
            statement.setInt(3, book.getPagesNumber());
            statement.setString(4, book.getDescription());
            statement.setString(5, book.getIsbn());
            statement.setString(6, book.getCover());
            int result = statement.executeUpdate();
            return (result > 0);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }

    public boolean editBook(Book book) throws DaoException {
        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(Resource.getValue(SQL_EDIT_BOOK))) {
            statement.setString(1, book.getName());
            statement.setString(2, book.getAuthor());
            statement.setInt(3, book.getPagesNumber());
            statement.setString(4, book.getDescription());
            statement.setString(5, book.getCover());
            statement.setInt(6, book.getBookId());
            int result = statement.executeUpdate();
            return (result > 0);
        } catch (ConnectionPoolException e) {
            throw new DaoException("", e);
        } catch (SQLException e) {
            throw new DaoException("", e);
        }
    }
}