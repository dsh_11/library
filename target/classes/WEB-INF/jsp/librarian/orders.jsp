<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 15.11.2015
  Time: 19:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="orders.title" var="title" />
<fmt:message key="orders.button.unconfirmed" var="unconfirmedButton" />
<fmt:message key="orders.button.confirmed" var="confirmedButton" />
<fmt:message key="orders.button.issued" var="issuedButton" />
<fmt:message key="orders.button.confirm" var="confirmButton" />
<fmt:message key="orders.button.issue" var="issueButton" />
<fmt:message key="orders.button.returned" var="returnedButton" />
<fmt:message key="orders.login" var="login" />
<fmt:message key="orders.book.name" var="bookName" />
<fmt:message key="orders.status" var="orderStatus" />
<fmt:message key="orders.type" var="type" />
<fmt:message key="orders.issue.date" var="issueDate" />
<fmt:message key="orders.return.date" var="returnDate" />

<html>
<head>
  <title>${title}</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
</head>
  <body>
    <jsp:include page="/jsp/part/nav.jsp" />
    <header class="header">
      <div class="container">
        <div class="order-pages">
          <ul class="pagination">
            <c:if test="${status == 'unconfirmed'}">
              <li class="active">
                <span id="page-active3">${unconfirmedButton}</span>
              </li>
              <li>
                <a id="page-inactive6" href="${pageContext.request.contextPath}/library?command=orders_list&status=confirmed">${confirmedButton}</a>
              </li>
              <li>
                <a id="page-inactive1" href="${pageContext.request.contextPath}/library?command=orders_list&status=issued">${issuedButton}</a>
              </li>
            </c:if>
            <c:if test="${status == 'confirmed'}">
              <li>
                <a id="page-inactive2" href="${pageContext.request.contextPath}/library?command=orders_list&status=unconfirmed">${unconfirmedButton}</a>
              </li>
              <li class="active">
                <span id="page-active1">${confirmedButton}</span>
              </li>
              <li>
                <a id="page-inactive3" href="${pageContext.request.contextPath}/library?command=orders_list&status=issued">${issuedButton}</a>
              </li>
            </c:if>
            <c:if test="${status == 'issued'}">
              <li>
                <a id="page-inactive4" href="${pageContext.request.contextPath}/library?command=orders_list&status=unconfirmed">${unconfirmedButton}</a>
              </li>
              <li>
                <a id="page-inactive5" href="${pageContext.request.contextPath}/library?command=orders_list&status=confirmed">${confirmedButton}</a>
              </li>
              <li class="active">
                <span id="page-active2">${issuedButton}</span>
              </li>
            </c:if>
          </ul>
        </div>

        <c:if test="${fn:length(orders) != 0}">
          <div class="list" style="margin-top: 120px">
            <table class="table table-hover">
              <thead>
              <tr>
                <th>${login}</th>
                <th>${bookName}</th>
                <th>${orderStatus}</th>
                <th>${type}</th>
                <c:if test="${status == 'issued'}">
                  <th>${issueDate}</th>
                  <th>${returnDate}</th>
                </c:if>
                <th></th>
              </tr>
              </thead>
              <tbody>
                <c:forEach items="${orders}" var="order">
                  <tr>
                    <td>${order.userLogin}</td>
                    <td><a href="${pageContext.request.contextPath}/library?command=book_info&bookId=${order.bookId}">${order.bookName}</a></td>
                    <td>${order.status}</td>
                    <td>${order.type}</td>
                    <c:if test="${status == 'unconfirmed'}">
                      <td>
                        <form action="${pageContext.request.contextPath}/library" method="post">
                          <input type="hidden" name="orderId" value="${order.orderId}" />
                          <input type="hidden" name="command" value="confirm_order" />
                          <input class="btn btn-md btn-black btn-block" type="submit" value="${confirmButton}" />
                        </form>
                      </td>
                    </c:if>
                    <c:if test="${status == 'confirmed'}">
                      <td>
                        <form action="${pageContext.request.contextPath}/library" method="post">
                          <input type="hidden" name="orderId" value="${order.orderId}" />
                          <input type="hidden" name="command" value="issue_order" />
                          <input class="btn btn-md btn-black btn-block" type="submit" value="${issueButton}" />
                        </form>
                      </td>
                    </c:if>
                    <c:if test="${status == 'issued'}">
                      <td>${order.issueDate}</td>
                      <td>${order.returnDate}</td>
                      <td>
                        <form action="${pageContext.request.contextPath}/library" method="post">
                          <input type="hidden" name="orderId" value="${order.orderId}" />
                          <input type="hidden" name="bookId" value="${order.bookId}" />
                          <input type="hidden" name="command" value="return_book" />
                          <input class="btn btn-md btn-black btn-block" type="submit" value="${returnedButton}" />
                        </form>
                      </td>
                    </c:if>
                  </tr>
                </c:forEach>
              </tbody>
            </table>
          </div>
        </c:if>


        <div class="pages">
          <ul class="pagination">
            <c:forEach begin="1" end="${pagesNumber}" var="i">
              <c:choose>
                <c:when test="${currentPage == i}">
                  <li class="active">
                    <span id="page-active">${i}</span>
                  </li>
                </c:when>
                <c:otherwise>
                  <li>
                    <a id="page-inactive" href="${pageContext.request.contextPath}/library?command=orders_list&status=${status}&page=${i}">${i}</a>
                  </li>
                </c:otherwise>
              </c:choose>
            </c:forEach>
          </ul>
        </div>
      </div>
    </header>
  </body>
</html>
