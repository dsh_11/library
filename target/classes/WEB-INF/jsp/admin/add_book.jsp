<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 22.11.2015
  Time: 10:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/tld/taglib.tld" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="add.book.title" var="title" />
<fmt:message key="add.book.name" var="name" />
<fmt:message key="add.book.author" var="author" />
<fmt:message key="add.book.pages.number" var="pagesNumber" />
<fmt:message key="add.book.isbn" var="isbn" />
<fmt:message key="add.book.amount" var="amount" />
<fmt:message key="add.book.description" var="description" />
<fmt:message key="add.book.button.add" var="addButton" />
<fmt:message key="add.book.wrong.data.message" var="wrongDataMessage" />
<fmt:message key="button.cancel" var="cancelButton" />
<fmt:message key="add.book.name.message" var="nameMessage" />
<fmt:message key="add.book.author.message" var="authorMessage" />
<fmt:message key="add.book.pages.number.message" var="pagesNumberMessage" />
<fmt:message key="add.book.isbn.message" var="isbnMessage" />
<fmt:message key="add.book.amount.message" var="amountMessage" />

<html>
  <head>
    <title>${title}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signin.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/validation.js"></script>
  </head>
  <body>
    <div class="container">
      <form class="form-signin" action="${pageContext.request.contextPath}/library?command=add_book" method="post"
            onsubmit="submitAddBook(this); return false;" enctype="multipart/form-data">
        <h2 class="form-register-heading">${title}</h2>
        <div id="name">
          <input class="form-control" type="text" name="name" id="nameInput" placeholder="${name}" />
        </div>
        <div id="author">
          <input class="form-control" type="text" name="author" id="authorInput" placeholder="${author}" />
        </div>
        <div id="pagesNumber">
          <input class="form-control" type="text" name="pagesNumber" id="pagesNumberInput" placeholder="${pagesNumber}" />
        </div>
        <div id="isbn">
          <input class="form-control" type="text" name="isbn" id="isbnInput" placeholder="${isbn}" />
        </div>
        <div id="amount">
          <input class="form-control" type="text" name="amount" id="amountInput" placeholder="${amount}" />
        </div>
        <div id="description">
          <textarea class="form-control" style="resize: none" rows="4" name="description" id="descriptionInput" placeholder="${description}"></textarea>
        </div>
        <div id="image">
          <input class="form-control" type="file" name="image" id="imageInput" />
        </div>
        <div class="message-box">
          <span class="message" id="nameMessage">${nameMessage}</span>
          <span class="message" id="authorMessage">${authorMessage}</span>
          <span class="message" id="pagesNumberMessage">${pagesNumberMessage}</span>
          <span class="message" id="isbnMessage">${isbnMessage}</span>
          <span class="message" id="amountMessage">${amountMessage}</span>
        </div>
        <c:if test="${wrongData}">
          <div>
            <span style="color: #a94442">${wrongDataMessage}</span>
          </div>
        </c:if>
        <div>
          <input class="btn btn-lg btn-black btn-block" type="submit" value="${addButton}" />
          <a class="btn btn-lg btn-black btn-block" href="${pageContext.request.contextPath}/library">${cancelButton}</a>
        </div>
      </form>
    </div>
  </body>
</html>