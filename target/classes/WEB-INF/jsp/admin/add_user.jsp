<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 17.11.2015
  Time: 20:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/tld/taglib.tld" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="add.user.title" var="title" />
<fmt:message key="authorization.first.name" var="firstName" />
<fmt:message key="authorization.last.name" var="lastName" />
<fmt:message key="authorization.email" var="email" />
<fmt:message key="authorization.login" var="login" />
<fmt:message key="authorization.password" var="password" />
<fmt:message key="authorization.password.confirm" var="passwordConfirm" />
<fmt:message key="add.user.role.librarian" var="roleLibrarian" />
<fmt:message key="add.user.role.admin" var="roleAdmin" />
<fmt:message key="add.user.button.add" var="addButton" />
<fmt:message key="authorization.sign.up.message.wrong.data" var="wrongDataMessage" />
<fmt:message key="authorization.sign.up.message.login.exists" var="loginExistsMessage" />
<fmt:message key="authorization.sign.up.message.first.name" var="firstNameMessage" />
<fmt:message key="authorization.sign.up.message.last.name" var="lastNameMessage" />
<fmt:message key="authorization.sign.up.message.email" var="emailMessage" />
<fmt:message key="authorization.sign.up.message.login" var="loginMessage" />
<fmt:message key="authorization.sign.up.message.password" var="passwordMessage" />
<fmt:message key="authorization.sign.up.message.confirm.password" var="confirmPassMessage" />
<fmt:message key="button.cancel" var="cancelButton" />

<html>
  <head>
    <title>${title}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signin.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/validation.js"></script>
  </head>
  <body>
    <div class="container">
      <form class="form-signin" action="${pageContext.request.contextPath}/library" method="post" onsubmit="submitAddUser(this); return false;">
        <tag:registerForm header="${title}" phFirstName="${firstName}" phLastName="${lastName}"
                          phEmail="${email}" phLogin="${login}"
                          phPassword="${password}" phPasswordConfirm="${passwordConfirm}"/>
        <div>
          <div>
            <label class="radio-inline">
              <input type="radio" name="optradio" value="librarian" checked="checked">${roleLibrarian}
            </label>
            <label class="radio-inline">
              <input type="radio" name="optradio" value="admin">${roleAdmin}
            </label>
          </div>
        </div>
        <div class="message-box">
          <span class="message" id="firstNameMessage">${firstNameMessage}</span>
          <span class="message" id="lastNameMessage">${lastNameMessage}</span>
          <span class="message" id="emailMessage">${emailMessage}</span>
          <span class="message" id="loginMessage">${loginMessage}</span>
          <span class="message" id="passwordMessage">${passwordMessage}</span>
          <span class="message" id="confirmPassMessage">${confirmPassMessage}</span>
        </div>
        <c:if test="${wrongData}">
          <div>
            <span style="color: #a94442">${wrongDataMessage}</span>
          </div>
        </c:if>
        <c:if test="${loginExists}">
          <div>
            <span style="color: #a94442">${loginExistsMessage}</span>
          </div>
        </c:if>
        <input type="hidden" name="role" />
        <input type="hidden" name="command" value="add_user" />
        <div>
          <input class="btn btn-lg btn-black btn-block" type="submit" value="${addButton}" />
          <a class="btn btn-lg btn-black btn-block" href="${pageContext.request.contextPath}/library">${cancelButton}</a>
        </div>
      </form>
    </div>
  </body>
</html>