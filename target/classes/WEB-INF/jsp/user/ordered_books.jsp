<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 15.11.2015
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="orders.title" var="title" />
<fmt:message key="orders.book.name" var="bookName" />
<fmt:message key="orders.status" var="orderStatus" />
<fmt:message key="orders.type" var="type" />
<fmt:message key="orders.issue.date" var="issueDate" />
<fmt:message key="orders.return.date" var="returnDate" />
<fmt:message key="orders.button.cancel" var="cancelButton" />

<html>
<head>
  <title>${title}</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.js"></script>
</head>
  <body>
    <jsp:include page="/jsp/part/nav.jsp" />
    <header class="header">
      <div class="container">
        <div class="list">
          <table class="table table-hover">
            <thead>
            <tr>
              <th>${bookName}</th>
              <th>${orderStatus}</th>
              <th>${type}</th>
              <th>${issueDate}</th>
              <th>${returnDate}</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${orders}" var="order">
              <tr>
                <td><a href="${pageContext.request.contextPath}/library?command=book_info&bookId=${order.bookId}">${order.bookName}</a></td>
                <td>${order.status}</td>
                <td>${order.type}</td>
                <td>
                  <c:if test="${order.issueDate != null}">
                    ${order.issueDate}
                  </c:if>
                  <c:if test="${order.issueDate == null}">
                    —
                  </c:if>
                </td>
                <td>
                  <c:if test="${order.returnDate != null}">
                    ${order.returnDate}
                  </c:if>
                  <c:if test="${order.returnDate == null}">
                    —
                  </c:if>
                </td>
                <td>
                  <c:if test="${order.status != 'issued'}">
                    <form action="${pageContext.request.contextPath}/library" method="post">
                      <input type="hidden" name="orderId" value="${order.orderId}" />
                      <input type="hidden" name="bookId" value="${order.bookId}" />
                      <input type="hidden" name="command" value="cancel_order" />
                      <input class="btn btn-md btn-black btn-block" type="submit" value="${cancelButton}" />
                    </form>
                  </c:if>
                </td>
              </tr>
            </c:forEach>
            </tbody>
          </table>
        </div>

        <div class="pages">
          <ul class="pagination">
            <c:forEach begin="1" end="${pagesNumber}" var="i">
              <c:choose>
                <c:when test="${currentPage == i}">
                  <li class="active">
                    <span id="page-active">${i}</span>
                  </li>
                </c:when>
                <c:otherwise>
                  <li>
                    <a id="page-inactive" href="${pageContext.request.contextPath}/library?command=ordered_books&page=${i}">${i}</a>
                  </li>
                </c:otherwise>
              </c:choose>
            </c:forEach>
          </ul>
        </div>
      </div>
    </header>
  </body>
</html>