<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 22.11.2015
  Time: 18:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="home.search.button" var="searchButton" />
<fmt:message key="home.search.input" var="searchInput" />
<fmt:message key="catalogue.book.name" var="bookName" />
<fmt:message key="catalogue.author" var="author" />
<fmt:message key="catalogue.add.book" var="addBook" />
<fmt:message key="catalogue.add.copies.button" var="addCopiesButton" />
<fmt:message key="catalogue.edit.button" var="editButton" />

<c:if test="${user.role == 'admin'}">
  <div style="padding-top: 40px; float: right;">
    <a class="btn btn-md btn-black" href="${pageContext.request.contextPath}/add-book">${addBook}</a>
  </div>
</c:if>
<form class="form-inline form-search" action="${pageContext.request.contextPath}/library" method="post" onsubmit="submitSearchForm(this); return false;">
  <div class="form-group form-search-input">
    <input class="form-control" style="width: 100%" type="text" name="searchQuery" placeholder="${searchInput}" value="${searchQuery}" />
  </div>
  <div class="form-group form-search-button">
    <input type="hidden" name="command" value="search" />
    <input class="form-control btn btn-md btn-black btn-block" type="submit" value="${searchButton}" />
  </div>
</form>
<div style="margin-top: 45px">
  <table class="table table-hover">
    <thead>
    <tr>
      <th></th>
      <th style="width: 60%">${bookName}</th>
      <th style="width: 35%">${author}</th>
      <c:if test="${user.role == 'admin'}">
        <th></th>
        <th></th>
      </c:if>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${books}" var="book">
      <tr>
        <td><img style="width: 30px" src="${book.cover}" /></td>
        <td><a href="${pageContext.request.contextPath}/library?command=book_info&bookId=${book.bookId}">${book.name}</a></td>
        <td>${book.author}</td>
        <c:if test="${user.role == 'admin'}">
          <td>
            <form action="${pageContext.request.contextPath}/library" method="post">
              <input type="hidden" name="bookId" value="${book.bookId}" />
              <input type="hidden" name="command" value="edit_book" />
              <input class="btn btn-md btn-black btn-block" style="width: auto" type="submit" value="${editButton}" />
            </form>
          </td>
          <td>
            <form action="${pageContext.request.contextPath}/library" method="post">
              <input type="hidden" name="bookId" value="${book.bookId}" />
              <input type="hidden" name="bookName" value="${book.name}" />
              <input type="hidden" name="command" value="add_copies" />
              <input class="btn btn-md btn-black btn-block" style="width: auto" type="submit" value="${addCopiesButton}" />
            </form>
          </td>
        </c:if>
      </tr>
    </c:forEach>
    </tbody>
  </table>
</div>
