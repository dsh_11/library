<%--
  Created by IntelliJ IDEA.
  User: Даша
  Date: 12.11.2015
  Time: 18:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.library.resource.prop" />
<fmt:message key="book.author" var="author" />
<fmt:message key="book.pages.number" var="pagesNumber" />
<fmt:message key="book.description" var="description" />
<fmt:message key="book.order.book" var="orderBook" />
<fmt:message key="book.message" var="message" />

<html>
  <head>
    <title>${book.name}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signin.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/stylesheet.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/book.css" />
  </head>
  <body>
    <%@ include file="../part/nav.jsp"%>
    <header class="header">
      <div class="container">
        <div class="book-header">
          <h1>${book.name}</h1>
          <div>
            <c:if test="${user.role != 'admin' && user.role != 'librarian'}">
              <form action="${pageContext.request.contextPath}/library" method="post">
                <input type="hidden" name="command" value="order_book" />
                <input type="hidden" name="bookId" value="${book.bookId}" />
                <input type="hidden" name="bookName" value="${book.name}" />
                <c:if test="${book.amount <= 0}">
                  <input class="btn btn-lg btn-black btn-block" disabled type="submit" value="${orderBook}" />
                </c:if>
                <c:if test="${book.amount > 0}">
                  <input class="btn btn-lg btn-black btn-block" type="submit" value="${orderBook}" />
                </c:if>
              </form>
              <p>${book.amount} ${message}</p>
            </c:if>
          </div>
        </div>

        <div class="book-img">
          <img src="${book.cover}" />
        </div>
        <div class="book-info">
          <table class="table">
            <tbody>
	            <tr>
	              <th scope="row">${author}</th>
	              <td>${book.author}</td>
	            </tr>
	            <tr>
	              <th scope="row">${pagesNumber}</th>
	              <td>${book.pagesNumber}</td>
	            </tr>
	            <tr>
	              <th scope="row">ISBN</th>
	              <td>${book.isbn}</td>
	            </tr>
	            <tr>
	              <th scope="row">${description}</th>
	              <td>${book.description}</td>
	            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </header>
  </body>
</html>
